import React from 'react'

import usePrevious from './usePrevious';

const useStateWithPrevious = (initial) => {
  const [state, setState] = React.useState(initial);
  return [state, setState, usePrevious(state)];
};

export default useStateWithPrevious