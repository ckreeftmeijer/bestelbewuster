import React from 'react'

import styled from 'styled-components'

import Card from '../Card'
import Container from '../Container'

import media from '../../theme/styledComponents/breakpoints';

const ContainerWithImage = ({ image, children }) => (
  <Container>
    <CardContainer>
      <Card>
        {children}
      </Card>
    </CardContainer>
    <Image src={image} />
  </Container>
)

const CardContainer = styled.div`
  width: 90%;
  margin: 0 auto;

  ${media.md`
    width: 50%;
    display: inline-block;
    vertical-align: middle;
    position: relative;
    left: 10%;
  `};
`


const Image = styled.img`
  display: none;

  ${media.md`
    width: 45%;
    display: inline-block;
    vertical-align: middle;
    box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    border-radius: 12px;
  `};
`

export default ContainerWithImage
