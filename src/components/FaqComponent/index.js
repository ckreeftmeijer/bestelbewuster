import React from 'react';
import { withTranslation } from 'react-i18next'

import Collapsable from '../../components/Collapsable'

import './styles.scss';

export class FAQ extends React.Component {
  state = {
    isOpen: undefined
  }
  render() {
    const { isOpen } = this.state
    const { items, title } = this.props;
    return (
      <div className="faq">
        <div className="home-container">
          <div className="h1 white bold">{title || 'Veelgestelde vragen'}</div>
          <br />
            {items.map((item, i) => (
              <Collapsable
                key={`collapse-${i}`}
                item={item}
                isOpen={isOpen === i}
                openCollapse={() => this.setState({isOpen: isOpen === i ? undefined : i})}
              />
            ))}
        </div>
      </div>
    )
  }
}


export default withTranslation()(FAQ)
