import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { margin } from '../../theme/styledComponents/mixins';

const StyledSpacer = styled.div`
  ${x => (x.vertical ? null : margin('right', x.amount))};
  ${x => (x.vertical ? margin('bottom', x.amount) : null)};
  display: ${x => (x.vertical ? 'block' : 'inline-block')};
`;

export const Spacer = ({ amount, vertical }) => (
  <StyledSpacer amount={amount} vertical={vertical} />
);

Spacer.propTypes = {
  amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  vertical: PropTypes.bool,
};

Spacer.defaultProps = {
  /** Amount of spacers */
  amount: 1,
  /** Determine if the space is vertical */
  vertical: false,
};

export default Spacer;
