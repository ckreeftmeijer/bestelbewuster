import React from 'react'
import { Link } from "react-router-dom";

import logoColor from "../../images/logo.png";
import Conditions from '../../files/Algemene_voorwaarden_2020_BestelBewuster_B.V..pdf'
import Privacy from '../../files/Privacy_Policy_BestelBewuster_B.V._2019.pdf'

import './styles.scss'

const Footer = ({ mobile }) => (
  <div id="footer" className="footer">
    <div className="footer-items">
      {!mobile && <div className="fourth hide-mobile" />}
      <div className="fourth">
        <Link to="/faq">Veelgestelde vragen</Link>
        <a href={Conditions} download>Algemene voorwaarden</a>
        <a href={Privacy} download>Privacy policy</a>
      </div>
      <div className="fourth email-links">
        <a href="mailto:info@bestelbewuster.nl"><i class="far fa-envelope" /> info@bestelbewuster.nl</a>
        <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/bestelbewuster"><i class="fab fa-linkedin" /> LinkedIn</a>
      </div>
      {!mobile && <div className="fourth hide-mobile" />}
    </div>
    <div className="copyright">
      <img
        className="header-logo"
        src={logoColor}
        alt="bestelbewuster wit logo"
      />
      <br /><br />
      © {new Date().getFullYear()} BestelBewuster, Alle rechten voorbehouden
    </div>
  </div>
)

export default Footer
