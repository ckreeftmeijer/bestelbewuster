import React from 'react'
import { Collapse } from 'react-collapse';

import './styles.scss'

class Collapsable extends React.Component {
  render() {
    const { item, isOpen } = this.props;
    return (
      <div className="collapsable pointer" onClick={() => this.props.openCollapse()}>
          <div className="col-title va-middle">
            {item.category && <div className="green small caps margin-v">{item.category}</div>}
            <h5 className="medium no-margin">{item.title}</h5>
          </div>
          <div className="col-plus text-right va-middle">
            <div className={`collapsable__plus ${isOpen ? 'collapsable__plus--open' : ''}`}>
              <span /><span />
            </div>
          </div>
        <Collapse isOpened={isOpen}>
          <br />
          <div className="small">{item.content}</div>
          </Collapse>
      </div>
    )
  }
}

export default Collapsable;
