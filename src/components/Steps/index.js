import React from 'react'
import styled, { css} from 'styled-components'
import Step1 from '../../images/Lunchinvite.svg'
import Step2 from '../../images/Email-BB.svg'
import Step3 from '../../images/acceptMeeting.png'
import Step4 from '../../images/Lunchinvite.svg'
import Step5 from '../../images/holding_ipad.png'
import Step6 from '../../images/bb-system.png'
import Step7 from '../../images/meeting.png'

import media from '../../theme/styledComponents/breakpoints';

import "./styles.scss";

const stepImages = [Step1,Step2,Step3,Step4,Step5,Step6,Step7]

const Step = styled.div`
  display: inline-block;
  width: ${x => 1 / x.width * 100}%;
  height: 200px;
  position: relative;

  & img {
    width: 60px;
    margin-top: 70px;
    margin-bottom: 70px;
    vertical-align: middle;
  }

  &:before {
    content: '';
    height: calc(50% - 12px);
    position: absolute;
    left: -6px;
    right: -6px;
  }

  &:after {
      top: 100%;
      border: solid transparent;
      content: " ";
      height: 0;
      width: 0;
      position: absolute;
      pointer-events: none;
      border-color: rgba(136, 183, 213, 0);
      border-width: 7px;
      z-index: 2;
  }

  ${x =>
    x.even ?
      css`
      &:before {
        border-bottom: 12px solid ${x => colors[x.index]};
        bottom: 12px;
        border-right: 12px solid ${x => colors[x.index]};
        border-left: 12px solid ${x => colors[x.index]};
      }

      &:after {
        top: calc(50% - 14px);
        right: 0px;
        margin-right: -7px;
        border-bottom-color: ${x => colors[x.index]};
      }

      ${StepText} {
        top: 220px;
      }

    `
    : css`
      &:before {
        border-top: 12px solid ${x => colors[x.index]};
        top: 12px;
        border-right: 12px solid ${x => colors[x.index]};
        border-left: 12px solid ${x => colors[x.index]};
      }

      &:after {
        top: 50%;
        right: -7px;
        margin-left: -7px;
        border-top-color: ${x => colors[x.index]};
      }

      ${StepText} {
        top: -120px;
      }
    `
  }
`

const StepText = styled.div`
  position: absolute;
  font-size: 1rem;

  @media(max-width: 768px) {
    top: 0px !important;
    font-size: 0.7rem;
    position: static;
    display: inline-block;
    vertical-align: middle;
    width: calc(100% - 120px);
    border-left: 12px solid ${x => colors[x.index]};
    margin-left: 12px;
    padding-left: 12px;
    height: 60px;
    margin-bottom: 6px;
    position: relative

    &:after {
      top: 100%;
    	left: -5px;
    	border: solid transparent;
    	content: " ";
    	height: 0;
    	width: 0;
    	position: absolute;
    	pointer-events: none;
    	border-color: rgba(136, 183, 213, 0);
    	border-top-color: ${x => colors[x.index]};
    	border-width: 7px;
    	margin-left: -7px;
    }
  }
`

const colors = ['#7BC255', '#4BAF99', '#f5ed7f', '#999999', '#7BC255', '#4BAF99', '#f5ed7f', '#7BC255']

const Steps = ({ steps }) => {
  return (
    <div className="process-steps">
      {steps.map((step, i) => (
        <Step
          className="step"
          width={steps.length}
          even={i % 2 === 0}
          index={i}
        >
          <img className="step-image" src={stepImages[i]} alt="step" />
          <StepText index={i}>{step}</StepText>
      </Step>
      ))}
    </div>
  )
}

export default Steps
