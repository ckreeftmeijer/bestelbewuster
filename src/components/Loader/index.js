import React from 'react'
import './styles.scss'

export const Loader = ({ className }) =>
  <div class={`loader ${className || ''}`}></div>


export default Loader
