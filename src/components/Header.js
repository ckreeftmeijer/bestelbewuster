import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import {  Link } from "react-router-dom";
import { Collapse } from 'react-collapse';
import styled from 'styled-components'

import Dropdown from './Dropdown'

import logoWhite from '../images/logo_wit.png';
import logoColor from '../images/logo.png';
import HamburgerWhite from '../images/hamburger_white.png';
import HamburgerGreen from '../images/hamburger_green.png';

import { useEventListener } from '../hooks'

const Header = ({ headerWhite, navOpen, mobile, toggleNav }) => {
  const [solutionsOpen, setSolutionsOpen] = useState()
  const [segmentsOpen, setSegmentsOpen] = useState()
  const [scrollPos,setScrollPos] = useState(0)

   const listenScrollEvent = e => {
     setScrollPos(window.scrollY)
   }

   const alwaysShow = scrollPos > 30

    useEventListener('scroll', listenScrollEvent);

  const segments = [
    { title: 'Co-workspace', link: '/segmenten/co-workspace'},
    { title: 'Hotels', link: '/segmenten/hotels'},
    { title: 'Leisure parken', link: '/segmenten/leisureparken'},
    { title: 'MKB', link: '/segmenten/mkb'},
  ]

  const solutions = [
    { title: 'Nieuwe normaal', link: '/oplossingen/nieuwe-normaal'},
    { title: 'Zakelijke evenementen', link: '/oplossingen/zakelijke-evenementen'},
  ]

  const { t } = useTranslation()
  const isHome = window.location.pathname === '/'
  return (
    <header className="App-header" style={{ backgroundColor: `${(headerWhite && !isHome) || alwaysShow ? 'white' : ''}`}}>
      <div className="bar">
        <div className="left">
          <Link to="/"><img className="header-logo" src={headerWhite || isHome ? logoColor : logoWhite} alt="bestelbewuster wit logo" />
          <div className="text-logo" style={{ color: `${headerWhite || isHome ? '#333' : 'white'}`}}>BestelBewuster</div></Link>
        </div>
        {mobile ? (
          <div className={`right ${headerWhite ? 'right--active' : ''}`}>
            <img onClick={() => toggleNav()} className="hamburger-icon" src={headerWhite ? HamburgerGreen : HamburgerWhite} alt="mobile icon" />
          </div>
        ) : (
          <div className={`right ${headerWhite ? 'right--active' : ''}`}>
            <Dropdown
              scroll={alwaysShow}
              title="Oplossingen"
              solutions={solutions}
            />
            <span style={{ color: 'white', margin: '0px 12px', fontSize: '14px' }}>|</span>
            <Dropdown
              scroll={alwaysShow}
              title="Segmenten"
              solutions={segments}
            />
            <StyledLink scroll={alwaysShow} to="/faq" className="bar-right">{t('faq')}</StyledLink>
            <StyledAnchor scroll={alwaysShow} href="mailto:info@bestelbewuster.nl" className="bar-right">Contact</StyledAnchor>
          </div>
        )}
      </div>
      <div onClick={() => toggleNav()} className={`navpanel ${navOpen ? 'navpanel--open' : ''}`}>
        <Link to="/" className="mobile-header-item">Homepage</Link>

        <div
          className="mobile-header-item"
          onClick={(e) => {e.stopPropagation(); setSolutionsOpen(!solutionsOpen)}}
        >
          Oplossingen
          <Collapse isOpened={solutionsOpen}>
            {solutions.map((sol, i) =>
              <Link key={i} to={sol.link} className="mobile-header-item__sub">{sol.title}</Link>
            )}
          </Collapse>
        </div>

        <div
          className="mobile-header-item"
          onClick={(e) => {e.stopPropagation(); setSegmentsOpen(!segmentsOpen)}}
        >
          Segmenten
          <Collapse isOpened={segmentsOpen}>
            {segments.map((seg, i) =>
              <Link key={i} to={seg.link} className="mobile-header-item__sub">{seg.title}</Link>
            )}
          </Collapse>
        </div>

        <Link to="/faq" className="mobile-header-item">{t('faq')}</Link>
      </div>
    </header>
  )
}

const StyledLink = styled(Link)`
  font-size: 0.9rem;
  margin-left: 24px;
  cursor: pointer;

  &:not(first-of-type) {
    &:before {
      content: '|';
      position: relative;
      left: -12px;
      color: white;
    }
  }

  &:hover {
    color: ${x => x.scroll ? '#7BC255' : '#f5ed7f'};
  }
`

const StyledAnchor = styled.a`
  font-size: 0.9rem;
  margin-left: 24px;
  cursor: pointer;

  &:not(first-of-type) {
    &:before {
      content: '|';
      position: relative;
      left: -12px;
      color: white;
    }
  }

  &:hover {
    color: ${x => x.scroll ? '#7BC255' : '#f5ed7f'};
  }
`

export default Header
