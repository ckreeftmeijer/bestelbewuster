import React from 'react'

import styled from 'styled-components'

import Arrow from './arrow.png'

import media from '../../theme/styledComponents/breakpoints';

const Pricing = () => (
  <Wrapper>
    <div className="green h3">Maandelijkse licentiekosten</div>
    <Prices className="container relative">
      <div className="col-6">
        <Price color="#41DBB3">Meetings</Price>
        <Price>Tot 25 meetings*</Price>
        <Price>Vanaf 25 tot 250 meetings</Price>
        <Price>Vanaf 250 meetings</Price>
      </div>
      <div className="col-6">
        <Price color="#4ABD82">Prijs</Price>
        <Price>€ 139 per maand </Price>
        <Price>€ 2 per meeting</Price>
        <Price>€ 1,75 per meeting</Price>
      </div>
      <Discount>
        <div style={{ lineHeight: '20px'}} className="bold larger">10% korting </div>
        <div>jaarlicentie</div>
      </Discount>
      <Basic>
        Basis licentie
      </Basic>
      <StyledArrow src={Arrow} />
    </Prices>
    <div className="half-width margin-auto block margin-bottom--lg">
      <span className="green bold h5">Gemiddeld: </span>
      1.000 medewerkers per locatie =  +/- 250 meetings per maand
    </div>
    <div className="gray small text-center">
      Starten met basislicentie voor € 139,- per maand. Hiervoor kunnen 25 meetings of 250 invites verstuurd worden. Extra meetings per maand worden afgerekend per meeting <br />
      * 1 meeting staat gelijk aan 10 invites
    </div>
  </Wrapper>
)



const Wrapper = styled.div`
  margin: 80px 0px;
`

const Basic = styled.div`
  display: none;
  font-family: 'Sriracha', cursive;
  position: absolute;
  left: -160px;
  top: 70px;
  fonts-size: 1.4rem;
  z-index: 2;
  transform: rotate(-10deg);

  ${media.md`
    display: block;
  `};
`

const StyledArrow = styled.img`
  display: none;
  position: absolute;
  left: -70px;
  top: 60px;
  width: 60px;
  transform: rotate(-30deg);

  ${media.md`
    display: block;
  `};
`

const Discount = styled.div`
  display: none
  position: absolute;
  right: -90px;
  top: -50px;
  height: 130px;
  width: 130px;
  border-radius: 130px;
  background: #55828B;
  color: white;
  padding: 34px;
  text-align: center;
  transform: rotate(10deg);

  &:before {
    right: 5px;
    top: 5px;
    border-radius: 120px;
    position: absolute;
    content: ' ';
    height: 120px;
    width: 120px;
    border: 4px solid white;
  }

  ${media.md`
    display: block;
  `};
`

const Prices = styled.div`
  max-width: 600px;
  margin: 40px auto;
  text-align: center;
`

const Price = styled.div`
  border-radius: 8px;
  background: ${x => x.color || '#fafafa'}
  color: ${x => x.color ? 'white' : 'black'}
  font-size: 1.2rem;
  padding: 12px;
  font-weight: ${x => x.color ? 'bold' : 'normal'};
  text-align: center;
  margin-bottom: 18px;
  box-shadow: 0 5px 12px rgba(0,0,0,0.05), 0 4px 8px rgba(0,0,0,0.12);
`

export default Pricing
