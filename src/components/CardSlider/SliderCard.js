import React from 'react'
import { darken } from 'polished'
import styled from 'styled-components'

// <SliderCard letter="P" index={4} title="Pretparken" />

const SliderCard = ({ title, letter, text, index, children, handleClick }) => (
  <Wrapper onClick={() => handleClick && handleClick()}>
    <Header>
      <Letter index={index}>{letter}</Letter>
      <Title>{title}</Title>
    </Header>
    <Body>{text || children}</Body>
  </Wrapper>
)

const Wrapper = styled.div`
  padding: 24px;
  cursor: ${x => x.onClick ? 'pointer' : 'normal'};
`

const Header = styled.div`

`

const Letter = styled.div`
  text-center;
  background: linear-gradient(to left top, ${x => colors[x.index]} 50%,  ${x => darken(0.2, colors[x.index])});
  height: 50px;
  width: 50px;
  border-radius: 6px;
  line-height: 50px;
  text-align: center;
  color: white;
  font-weight: bold;
  font-size: 2rem;
  display: inline-block;
`

const Title = styled.div`
  display: inline-block;
  margin-left: 12px;
  font-size: 1.3rem;
`

const Body = styled.div`
  margin-top: 12px;
`

const colors = [
  '#4ABD82',
  '#47BD81',
  '#41DBB3',
  '#4ABD82',
  '#f5ed7f',
  '#47BD81',
  '#F6AF2A',
  '#41DBB3',
  '#4ABD82',
  '#47BD81',
  '#41DBB3',
  '#4ABD82',
  '#f5ed7f',
  '#47BD81',
  '#F6AF2A',
  '#41DBB3',
]


export default SliderCard
