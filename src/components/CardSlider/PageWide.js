import React, { useState, useEffect} from 'react'

import styled, { keyframes } from 'styled-components'

import { usePrevious } from '../../hooks'

import LeftWhite from '../../images/icons/left.svg'
import LeftColor from '../../images/icons/left_color.svg'

const CardSlider = ({ children }) => {
  const [offset, setOffset] = useState(0)

  const changeOffset = (minus) => {
    let newOff = offset

    if (minus) {
      newOff -= 1
    } else {
      newOff += 1
    }

    if (newOff > 5) {
      newOff = 0
    }
    if (newOff < 0) {
      newOff = 5
    }
    setOffset(newOff)
  }


  return (
    <Wrapper>
      {children.map((card, i) => (
        <SlideCard index={i} status={i + offset > 5 ? i + offset - 6 : i + offset}>
          {card}
        </SlideCard>
      ))}
      <ButtonContainer>
        <RoundButton onClick={() => changeOffset(true)}>
          <Arrow src={LeftWhite} alt="arrow left"/>
        </RoundButton>
        <RoundButton color onClick={() => changeOffset(false)}>
          <Arrow src={LeftColor} className="mirror" alt="arrow right"/>
        </RoundButton>
      </ButtonContainer>
    </Wrapper>
  )
}

const SlideCard = ({ status, children, goToCard }) => {
  const [animation, setAnimation] = useState('')
  const prevStatus = usePrevious(status)
  const animations = [none, one, two, three, four, five]
  const animationsBack = [noneBack, oneBack, twoBack, threeBack, fourBack, fiveBack]
  const handleAnimation = (status) => {
    if (status < 5 && status > -2) {
      if (prevStatus + 1 === status || !prevStatus) {
        return setAnimation(animations[status])
      }
      if (prevStatus - 1 === status || !prevStatus) {
        return setAnimation(animationsBack[status])
      }
    }
    else {
      setAnimation(none)
    }
  }

  useEffect(() => {
    if (prevStatus !== status) {
      handleAnimation(status)
    }
  }, [status, prevStatus])
  return (
    <StyledCard animation={animation}>
      {children}
    </StyledCard>
  )
}

const Wrapper = styled.div`
  position: relative;
  height: 400px;
`

const RoundButton = styled.div`
  display: inline-block;
  margin: 6px;
  height: 36px;
  width: 36px;
  border-radius: 50%;
  line-height: 36px;
  background: ${x => x.color ? 'white' : 'linear-gradient(45deg, #7BC255, #4BAF99)'};
  box-shadow: 0 0 2px ${x => x.color ? '#4BAF99' : 'white'};
  text-align: center;
  cursor: pointer;

  &:hover {
    box-shadow: 0 8px 12px rgba(75,175,153,0.25), 0 10px 10px rgba(75,175,153,0.22);
  }
`

const Arrow = styled.img`
  width: 75%;
`

const ButtonContainer = styled.div`
  position: absolute;
  top: 320px;
  right: calc(50% - 50px);
`

const StyledCard = styled.div`
  width: 40%;
  height: 200px;
  animation: ${x => x.animation} 0.5s ease-in;
  position: absolute;
  left: 0;
  top: 90px;
  overflow: hidden;
  padding: 12px;
  border-radius: 12px;
  box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
  background: white;
  display: flex;
  flex-direction: column;
  min-height: 40px;
  margin-bottom: 6px;
  animation-fill-mode: forwards;
`


const none = keyframes`
  from {
    transform: translateX(-999vw);
  }

  to {
    transform: translateX(-999vw);
  }
`



const one = keyframes`
  from {
    transform: translateX(-999vw);
  }

  to {
    transform: translateX(-100%);
  }
`

const two = keyframes`
  from {
    transform: translateX(-100%);
  }

  to {
    transform: translateX(75%);
  }
`

const three = keyframes`
  from {
    transform: translateX(75%);
  }

  to {
    transform: translateX(250%);
  }
`

const four = keyframes`
  from {
    transform: translateX(250%);
  }

  to {
    transform: translateX(999vw);
  }
`

const five = keyframes`
  from {
    transform: translateX(90vw);
  }

  to {
    transform: translateX(1000vw);
  }
`


const noneBack = keyframes`
  from {
    transform: translateX(-999vw);
  }

  to {
    transform: translateX(-999vw);
  }
`



const oneBack = keyframes`
  from {
    transform: translateX(-100%);
  }

  to {
    transform: translateX(-999vw);
  }
`

const twoBack = keyframes`
  from {
    transform: translateX(75%);
  }

  to {
    transform: translateX(-100%);
  }
`

const threeBack = keyframes`
  from {
    transform: translateX(250%);
  }

  to {
    transform: translateX(75%);
  }
`

const fourBack = keyframes`
  from {
    transform: translateX(999vw);
  }

  to {
    transform: translateX(250%);
  }
`

const fiveBack = keyframes`
  from {
    transform: translateX(1000vw);
  }

  to {
    transform: translateX(90vw);
  }
`

export default CardSlider
