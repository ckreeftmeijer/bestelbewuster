import React, { useState, useEffect} from 'react'

import styled, { keyframes } from 'styled-components'

import { usePrevious } from '../../hooks'

import LeftWhite from '../../images/icons/left.svg'
import LeftColor from '../../images/icons/left_color.svg'

const CardSlider = ({ children }) => {
  const [offset, setOffset] = useState(0)

  const changeOffset = (minus) => {
    let newOff = offset

    if (minus) {
      newOff -= 1
    } else {
      newOff += 1
    }

    if (newOff > 6) {
      newOff = 0
    }
    if (newOff < 0) {
      newOff = 6
    }
    setOffset(newOff)
  }


  return (
    <Wrapper>
      {children.map((card, i) => (
        <SlideCard index={i} status={i + offset > 6 ? i + offset - 7 : i + offset}>
          {card}
        </SlideCard>
      ))}
      <ButtonContainer>
        <RoundButton onClick={() => changeOffset(true)}>
          <Arrow src={LeftWhite} alt="arrow left"/>
        </RoundButton>
        <RoundButton color onClick={() => changeOffset(false)}>
          <Arrow src={LeftColor} className="mirror" alt="arrow right"/>
        </RoundButton>
      </ButtonContainer>
    </Wrapper>
  )
}

const SlideCard = ({ status, children, goToCard }) => {
  const [animation, setAnimation] = useState('')
  const prevStatus = usePrevious(status)
  const animations = [none, one, two, three, four, five, six]
  const animationsBack = [noneBack, oneBack, twoBack, threeBack, fourBack, fiveBack, sixBack]
  const handleAnimation = (status) => {
    if (status < 6 && status > -2) {
      if (prevStatus + 1 === status || !prevStatus) {
        return setAnimation(animations[status])
      }
      if (prevStatus - 1 === status || !prevStatus) {
        return setAnimation(animationsBack[status])
      }
    }
    else {
      setAnimation(none)
    }
  }

  useEffect(() => {
    if (prevStatus !== status) {
      handleAnimation(status)
    }
  }, [status, prevStatus])
  return (
    <StyledCard animation={animation}>
      {children}
    </StyledCard>
  )
}

const Wrapper = styled.div`
  position: relative;
`

const RoundButton = styled.div`
  display: inline-block;
  margin: 6px;
  height: 36px;
  width: 36px;
  border-radius: 50%;
  line-height: 36px;
  background: ${x => x.color ? 'white' : 'linear-gradient(45deg, #7BC255, #4BAF99)'};
  box-shadow: 0 0 2px ${x => x.color ? '#4BAF99' : 'white'};
  text-align: center;
  cursor: pointer;

  &:hover {
    box-shadow: 0 8px 12px rgba(75,175,153,0.25), 0 10px 10px rgba(75,175,153,0.22);
  }
`

const Arrow = styled.img`
  width: 75%;
`

const ButtonContainer = styled.div`
  z-index: 10;
  position: absolute;
  top: 210px;
  right: -100px;
`

const StyledCard = styled.div`
  width: 40%;
  height: 200px;
  animation: ${x => x.animation} 0.5s ease-in;
  position: absolute;
  left: 0;
  top: 0;

  overflow: hidden;
  padding: 12px;
  border-radius: 12px;
  box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
  background: white;
  display: flex;
  flex-direction: column;
  min-height: 40px;
  margin-bottom: 6px;
  animation-fill-mode: forwards;
  z-index: 2;
  opacity: 1;
`

const none = keyframes`
  from {
    transform: translateX(70%) translateY(30%);
    z-index: 0;
    opacity: 0;
  }

  to {
    z-index: 1;
    opacity: 0;
    transform: translateX(70%) translateY(30%);
  }
`



const one = keyframes`
  from {
    z-index: 1;
    transform: translateX(70%) translateY(30%);
    opacity: 0;
  }

  to {
    opacity: 1;
    z-index: 2;
    transform: translateX(80%) translateY(20%);
  }
`

const two = keyframes`
  from {
    z-index: 2;
    transform: translateX(80%) translateY(20%);
  }

  to {
    z-index: 3;
    transform: translateX(90%) translateY(10%);
  }
`

const three = keyframes`
  from {
    z-index: 3;
    transform: translateX(90%) translateY(10%);
  }

  to {
    z-index: 4;
    transform: translateX(100%) translateY(0%);
  }
`

const four = keyframes`
  from {
    z-index: 4;
    transform: translateX(100%) translateY(0%);
  }

  to {
    z-index: 5;
    transform: translateX(220%) translateY(0%);
  }
`

const five = keyframes`
  from {
    z-index: 5;
    transform: translateX(220%) translateY(0%);
  }

  to {
    z-index: 6;
    // transform: translateX(330%) translateY(0%);
    transform: translateX(200vw) translateY(0%);
  }
`

const six = keyframes`
  from {
    z-index: 6;
    transform: translateX(330%) translateY(0%);
  }

  to {
    z-index: 7;
    transform: translateX(120vw) translateY(0%);
    opacity: 0;
  }
`

const noneBack = keyframes`
  from {
    opacity: 0;
    z-index: 6;
    transform: translateX(120vw) translateY(0%);
  }
  to {
    transform: translateX(120vw) translateY(0%);
    opacity: 1;
  }
`

const oneBack = keyframes`
  from {
    transform: translateX(80%) translateY(20%);
    z-index: 1;
    opacity: 1;
  }

  to {
    z-index: 0;
    opacity: 0;
    transform: translateX(70%) translateY(30%);
  }
`

const twoBack = keyframes`
  from {
    z-index: 2;
    transform: translateX(90%) translateY(10%);
  }

  to {
    z-index: 1;
    transform: translateX(80%) translateY(20%);
  }
`

const threeBack = keyframes`
  from {
    z-index: 4;
    transform: translateX(100%) translateY(0%);
  }

  to {
    z-index: 3;
    transform: translateX(90%) translateY(10%);
  }
`

const fourBack = keyframes`
  from {
    z-index: 5;
    transform: translateX(220%) translateY(0%);
  }

  to {
    z-index: 4;
    transform: translateX(100%) translateY(0%);
  }
`

const fiveBack = keyframes`
  from {
    z-index: 6;
    transform: translateX(330%) translateY(0%);
  }

  to {
    z-index: 5;
    transform: translateX(220%) translateY(0%);
  }
`

const sixBack = keyframes`
  from {
    opacity: 1;
    z-index: 7;
    transform: translateX(120vw) translateY(0%);
  }

  to {
    z-index: 6;
    transform: translateX(330%) translateY(0%);
    opacity: 1;
  }
`

export default CardSlider
