import React from 'react'

import styled from 'styled-components'

const Button = ({ title, children, circle, idx = 0}) => (
  <Card>
    {circle && <Circle idx={idx} />}
    <Content circle={circle}>
      <Title idx={idx}>{title}</Title>
      <Text>{children}</Text>
    </Content>
  </Card>
)

const colors = [
  '#4ABD82',
  '#47BD81',
  '#4ABD82',
  '#4ABD82',
]

const Card = styled.div`
  overflow: hidden;
  position: relative;
  padding: 24px 12px;
  border-radius: 4px;
  box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
  background: white;
  display: flex;
  flex-direction: column;
  min-height: 40px;
  margin-bottom: 6px;
  height: 100%;
`

const Circle = styled.div`
  position: absolute;
  left: -50px;
  top: calc(50% - 60px);
  width: 120px;
  height: 120px;
  border-radius: 50%;
  border: 5px solid ${x => colors[x.idx]};
`

const Content = styled.div`
  margin-left: ${x => x.circle ? '15%' : '0px'};
  padding: 12px;
`

const Title = styled.div`
  font-weight: 600;
  color: ${x => colors[x.idx]};
  font-size: 1.3rem;
`

const Text = styled.div`

`

export default Button
