import React from 'react'
import { Formik, Field } from 'formik'
import { object, string } from "yup";
import { toast } from 'react-toastify';

import Card from '../../Card'
import Button from '../../Button'
import ErrorMessage from '../ErrorMessage'

import HubspotService from '../../../services/HubspotService';

const hubspot = new HubspotService()

const submitForm = (values, actions, page) => {
  const { naam, organisatie, email } = values
  hubspot.submitForm({
    formId: 'ced27481-e893-41ad-be76-62076847cb4c',
    fields: [
      {
        name: 'naam',
        value: naam
      },
      {
        name: 'organisatie',
        value: organisatie
      },
      {
        name: 'email',
        value: email
      },
      {
        name: 'pagina',
        value: page
      },
    ]
  })
  .then(res => {
      actions.setSubmitting(false)
      actions.resetForm()
      toast('Bedankt voor je aanmelding! Wij nemen contact met je op.', {
        type: 'success',
        autoClose: 5000,
      })
  }, err => {
    actions.setSubmitting(false)
    toast('Er is helaas iets mis gegaan. Probeer het opnieuw', {
      type: 'error',
      autoClose: 5000,
    })
  })
}

const Basic = ({page}) => (
  <Card>
    <div className="hubspot black">
      <div className="green bold h3 text-center">MEER WETEN?<br /> NEEM CONTACT OP</div>
      <Formik
          onSubmit={(values, actions) => {
            submitForm(values, actions, page)
          }}
          initialValues={{
            naam: '',
            organisatie: '',
            email: '',
          }}
          validationSchema={Schema}
          render={({
            errors,
            touched,
            handleSubmit,
            isSubmitting,
            values
          }) => (
            <form autoComplete="off" onSubmit={handleSubmit}>

              <div className={`field-container`}>
                {errors.naam && touched.naam && <ErrorMessage name="naam" />}
                <Field type="text" name="naam"/>
                <span className={`placeholder ${values.naam !== '' ? 'placeholder--active' : ''}`}>Naam</span>
              </div>

              <div className={`field-container`}>
                {errors.organisatie && touched.organisatie && <ErrorMessage name="organisatie" />}
                <Field type="text" name="organisatie"/>
                <span className={`placeholder ${values.organisatie !== '' ? 'placeholder--active' : ''}`}>Organisatie</span>
              </div>

              <div className={`field-container`}>
                {errors.email && touched.email && <ErrorMessage name="email" />}
                <Field type="text" name="email"/>
                <span className={`placeholder ${values.email !== '' ? 'placeholder--active' : ''}`}>E-mail</span>
              </div>

              <Button type="submit" disabled={isSubmitting}>
                {isSubmitting ? 'loading..' : 'Aanmelden'}
              </Button>
            </form>
          )}
        />
      </div>
  </Card>
)

const Schema = object().shape({
  naam: string().required("Verplicht"),
  organisatie: string().required("Verplicht"),
  email: string().required("Verplicht"),
});

export default Basic
