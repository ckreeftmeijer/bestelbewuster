import React from 'react';
import { ErrorMessage } from "formik";

import './styles.scss';

const errorMessage = ({ name }) => (
  <span className="formik-error">
    <ErrorMessage name={name} />
  </span>
);

export default errorMessage;
