import React from 'react'
import styled from 'styled-components'

import Button from '../Button'

const CTA = ({ title, href, inverted, to }) => (
  <Wrapper inverted={inverted}>
    <StyledContainer
      inverted={inverted}
    >
      <Title
        inverted={inverted}
      >
        {title}
      </Title>
      <SubTitle>Exclusief eenmalige implementatiekosten</SubTitle>
      <br />
      <Button
        href={href}
        to={to}
        inline
        title="Meer weten?"
      />
    </StyledContainer>
  </Wrapper>
)

const Wrapper = styled.div`
  background: ${x => x.inverted ? '#1e8a7b' : '#white'};
  padding: 80px 0px;
`

const SubTitle = styled.div`
  font-size: 0.9rem;
  color: rgba(255,255,255,0.8);
`

const StyledContainer = styled.div`
  color: ${x => x.inverted ? 'white' : '#1e8a7b'};
  text-align: center;
  width: 700px;
  margin: 0 auto;
  font-size: 1.4rem;
`

const Title = styled.div`
  font-weight: bold;
`

export default CTA
