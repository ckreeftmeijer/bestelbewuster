import React from 'react'

import styled from 'styled-components'

const LeftRight = ({ background, direction, colored, children }) => {
  return (
    <StyledContainer
    >
      <Left
        background={background}
        direction={direction}
      >
        <LeftContent>
          {direction === 'right'
            ? children
            : <Title>{colored}</Title>}
        </LeftContent>
      </Left>
      <Right
        background={background}
        direction={direction}
      >
        <RightContent>
          {direction === 'left'
            ? children
            : <Title>{colored}</Title>}
        </RightContent>
       </Right>
    </StyledContainer>
  )
}

const StyledContainer = styled.div`
  display: flex;
`

const Left = styled.div`
  flex: 1;
  background: ${x => x.direction === 'left' ? x.background : 'white'};
  color: ${x => x.direction === 'left' ? 'white' : 'inherit'};
  min-height: 120px;
  padding: 60px 30px;
  display: flex;
  align-items: center;
`

const Right = styled.div`
  flex: 1;
  background: ${x => x.direction === 'right' ? x.background : 'white'};
  color: ${x => x.direction === 'right' ? 'white' : 'inherit'};
  min-height: 120px;
  padding: 60px 30px;
  display: flex;
  align-items: center;
`

const LeftContent = styled.div`
  max-width: 800px;
  width: 100%;
`

const RightContent = styled.div`
  max-width: 800px;
  width: 100%;
`

const Title = styled.div`
  text-align: center;
  font-size: 1.4rem;
  font-weight: bold;
`

LeftRight.defaultProps = {
  background: '#4BAF99'
}

export default LeftRight
