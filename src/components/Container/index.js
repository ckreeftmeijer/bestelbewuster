import React from 'react'

import styled from 'styled-components'

import media from '../../theme/styledComponents/breakpoints';

const Container = ({ children }) =>
  <StyledContainer>
    {children}
  </StyledContainer>


const StyledContainer = styled.div`
  position: relative;
  max-width: 1200px;
  margin: 0 auto;
  text-align: left;
  padding: 24px;

  ${media.lg`
    padding: 0px;
  `};
`

export default Container
