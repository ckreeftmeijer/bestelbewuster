import React from 'react'
import styled from 'styled-components'

import Button from '../Button'

const CTA = ({ title, href, inverted }) => (
  <Wrapper inverted={inverted}>
    <StyledContainer
      inverted={inverted}
    >
      <Title
        inverted={inverted}
      >
        {title}
      </Title>
      <br />
      <Button
        href={href}
        inline
        title="Neem contact op"
      />
    </StyledContainer>
  </Wrapper>
)

const Wrapper = styled.div`
  background: ${x => x.inverted ? '#1e8a7b' : '#white'};
  padding: 80px 0px;
`

const StyledContainer = styled.div`
  color: ${x => x.inverted ? 'white' : '#1e8a7b'};
  text-align: center;
  width: 500px;
  margin: 0 auto;
  font-weight: bold;
  font-size: 1.4rem;
`

const Title = styled.div`

`

export default CTA
