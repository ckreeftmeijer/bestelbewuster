import React from 'react'

import styled from 'styled-components'

import media from '../../theme/styledComponents/breakpoints';

const SimpleSteps = ({ steps }) => (
  <Wrapper>
    {steps.map((step, i) => (
      <Step key={i}>
        <Arrow
          isOdd={Boolean(i % 2 === 0)}
          zIdx={10 - i}
        >
          <Title>
            <Number>{i + 1}. </Number>
            {step.title}
          </Title>
        </Arrow>
        <Text>{step.text}</Text>
      </Step>
    ))}
  </Wrapper>
)


const Wrapper = styled.div`
  display: flex;
  padding: 12px;
  flex-direction: column;

  ${media.lg`
    flex-direction: row;
  `};
`

const Step = styled.div`
  flex: 1;
`

const Title = styled.span`
  ${media.lg`
    position: relative;
    left: 20px;
  `};
`

const Text = styled.div`
  width: 80%;
  margin: 18px auto;
  font-size: 0.9rem;
  z-index: 20;
`

const Number = styled.span`
  ${media.lg`
    display: none;
  `};
`

const Arrow = styled.div`
  background: #47BD81;
  color: white;
  text-align: center;
  position: relative;
  height: 70px;
  line-height: 70px;
  font-weight: bold;
  font-size: 1.2rem;

  ${media.lg`
    background: ${x => x.isOdd ? '#fafafa' : '#47BD81' };
    color: ${x => x.isOdd ? '#47BD81' : 'white' };

    &:after {
      content: "";
      position: absolute;
      width: 0;
      height: 0;
      right: -34px;
      border-top: 35px solid transparent;
      border-bottom: 35px solid transparent;
      border-left: 35px solid ${x => x.isOdd ? '#fafafa' : '#47BD81' };
      z-index: ${x => x.zIdx};
    }
  `};
`

export default SimpleSteps
