import React from 'react'
import { useTranslation } from 'react-i18next'
import './styles.scss'
import NL from './nl.svg'
import EN from './en.svg'

const LanguageSelector = () => {
  const { i18n } = useTranslation()

  const changeLanguage = (lang) => {
    i18n.changeLanguage(lang)
  }

  const currentLang = i18n.language;

  return (
    <img
      className="country-flag" src={currentLang === 'en' ? EN : NL}
      alt="country flag"
      onClick={() => changeLanguage(currentLang === 'nl' ? 'en' : 'nl')}
    />
  )
}

export default LanguageSelector
