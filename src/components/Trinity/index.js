import React from 'react'

import styled from 'styled-components'

import media from '../../theme/styledComponents/breakpoints';

const Trinity = ({ children }) => {
  return (
    <Container>
      <div className="container padded--lg margin-v--lg">
        {children.map((val, i) => {
          const hasCircle = val.img || val.circleText
          return (
            <ItemContainer
              key={i}
              className="col-12 col-lg-4"
            >
              {
                hasCircle && (
                  <Circle className="usp-title">
                    {val.img && <Icon height={val.height} src={val.img} />}
                    {val.circleText && <div>{val.circleText}</div>}
                  </Circle>
                )
              }
              <SubTitle>
                <Title hasCircle={hasCircle}>{val.title}</Title>
                <span>{val.text}</span>
              </SubTitle>
            </ItemContainer>
          )
        })}
      </div>
    </Container>
  )
}

const Container = styled.div`
  margin: 40px 0px;
`

const Circle = styled.div`
  font-size: 2rem;
  color: #4BAF99;
  font-weight: bold;
  border: 5px solid #4BAF99;
  border-radius: 50%;
  height: 100px;
  width: 100px;
  text-align: center;
  line-height: 100px;
  margin: 0 auto;
  margin-bottom: 24px;
`

const Icon = styled.img`
  height: ${x => x.height || '100%'};
  position: relative;
  top: -10px;
`

const SubTitle = styled.div`
  font-size: 1.2rem;
  color: #333;
  font-weight: bold;
  text-align: center;


  & span {
    display: block;
    font-weight: normal;
    font-size: 0.9rem;

  }
`

const ItemContainer = styled.div`
  padding: 0px 6px;
  margin: 0px 16px;
`

const Title = styled.div`
  border-bottom: ${x => x.hasCircle ? 'none' : '3px solid #4BAF99'};
  margin-bottom: ${x => x.hasCircle ? 'none' : '20px'};
  height: 70px;
`

export default Trinity
