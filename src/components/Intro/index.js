import React from 'react'

import styled from 'styled-components'

import WidthContainer from '../Container'
import Basic from '../Hubspot/Basic'

import media from '../../theme/styledComponents/breakpoints';

const Intro = ({ title, children, image, form }) =>
  <Container>
    <WidthContainer>
      <div className="container text-left">
        <div className="col-12 col-lg-6">
          <Title>{title}</Title>
          {children}
          <Image src={image} />
        </div>
        <div className="col-12 col-lg-6">
          <SideContainer>
            <Basic page={form} />
          </SideContainer>
        </div>
      </div>
    </WidthContainer>
  </Container>


const Container = styled.div`
  color: white;
  background: linear-gradient(45deg, #7BC255, #4BAF99);
  min-height: 120px;
  padding: 60px 0px;
  font-size: 1.2rem;
  text-align: center;
`

const Title = styled.h1`
  font-weight: bold;
  text-transform: uppercase;
  font-size: 2rem;
  display: inline-block;
  margin-bottom: 24px;
  margin-top: 40px;
`

const Image = styled.img`
  width: 70%;
  margin-left: 20%;
  display: none;

  ${media.lg`
    display: block;
  `};
`
const SideContainer = styled.div`
  padding-top: 40px;
  max-width: 400px;
  margin: 0 auto;
`

export default Intro
