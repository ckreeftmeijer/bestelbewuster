import React from 'react'

import { Link } from "react-router-dom";
import styled from 'styled-components'

const SolutionDropdown = ({ scroll, solutions, title }) => {
  return (
      <Title scroll={scroll}>
        {title}
        <Dropdown>
          {solutions.map(sol => (
            <Item to={sol.link}>{sol.title}</Item>
          ))}
        </Dropdown>
      </Title>
  )
}


const Dropdown = styled.div`
  position: absolute;
  box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
  background: white;
  top: 30px;
  width: 260px;
  border-radius: 4px;
  max-height: 0px;
  transition: max-height ease 0.3s;
  overflow: hidden;
`

const Title = styled.div`
  position: relative;
  display: inline-block;
  color: ${x => x.scroll ? '#4BAF99' : 'white'};
  font-size: 0.9rem;

  &:hover {
    color: ${x => x.scroll ? '#7BC255' : '#f5ed7f'};
  }

  &:hover ${Dropdown} {
    max-height: 400px;
  }
`

const Item = styled(Link)`
  border-bottom: 1px solid #4BAF99;
  padding: 12px;
  color: #4BAF99 !important;
  display: block;

  &:hover {
    background: #fafafa;
  }

  &:last-of-type {
    border-bottom: none;
  }
`

export default SolutionDropdown
