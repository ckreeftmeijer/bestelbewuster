import React from 'react'
import {  Link } from "react-router-dom";

import styled from 'styled-components'

const Button = ({ title, children, inline, href, to, ...props }) => (
  <StyledButton
    {...props}
    inline={inline}
  >
    {
      href
        ? <a href={href}>{title}</a>
        : to
            ? <Link to={to}>{title}</Link>
            : <div>{title || children}</div>
    }
  </StyledButton>
)

const StyledButton = styled.button`
  display: ${x => x.inline ? 'inline-block' : 'block'};
  width: ${x => x.inline ? 'auto' : '100%'};
  vertical-align: middle;
  text-align: center;
  padding: 12px 24px;
  font-weight: bold;
  font-size: 16px;
  color: white;
  border-radius: 4px;
  cursor: pointer;
  background: linear-gradient(45deg, #1e8a7b, #f5ed7f);

  & a {
    color: white;
  }
`

export default Button
