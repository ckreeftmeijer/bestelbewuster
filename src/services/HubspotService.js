import axios from "axios";

class HubspotService {

  submitForm = ({ fields, formId, communications }) => {
    const portalId = '7012626'

    const body = {
      fields
    }

    if (communications) {
      body.legalConsentOptions = {
        "consent":{
          "consentToProcess":true,
          // Boolean; Whether or not the visitor checked the Consent to process checkbox
          "text":"Text that gives consent to process",
          // String; The text displayed to the visitor for the Consent to process checkbox
          "communications":[
            {
               "value": true,
               "subscriptionTypeId": '8217954',
               "text": "I agree to receive other communications from BestelBewuster."
            }
          ]
        }
      }
    }

    return axios.post(`https://api.hsforms.com/submissions/v3/integration/submit/${portalId}/${formId}`, body).then(
      data => data.data,
      err => Promise.reject(err)
    )
  }

}

export default HubspotService;
