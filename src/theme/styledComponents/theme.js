//
// ████████╗██╗  ██╗███████╗███╗   ███╗███████╗
// ╚══██╔══╝██║  ██║██╔════╝████╗ ████║██╔════╝
//    ██║   ███████║█████╗  ██╔████╔██║█████╗
//    ██║   ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══╝
//    ██║   ██║  ██║███████╗██║ ╚═╝ ██║███████╗
//    ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚══════╝
//

export const containers = {
  sm: "395px",
  md: "738px",
  lg: "994px",
  xl: "1170px"
};

export const breakpointsMin = [
  "425px",
  "768px",
  "1024px",
  "1280px",
  "1366px",
  "1440px",
  "1900px"
];

export const breakpointsMax = [
  "424px",
  "767px",
  "1023px",
  "1279px",
  "1365px",
  "1439px",
  "1899px"
];

export const space = [0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60]; // used by '@rebass/grid'

export const spacer = "6px";

export const colors = {
  /*
  █▄░█ █▀▀ █░░░█ ▀▀▀▀█ ▄▀▀▄ ▄▀▀▄
  █░▀█ █▀▀ █░█░█ ░▄▀▀░ █░░█ █░░█
  ▀░░▀ ▀▀▀ ░▀░▀░ ▀▀▀▀▀ ░▀▀░ ░▀▀░  */
  nzGreenLighter: "#b2e4c0",
  nzGreenLight: "#7fd396",
  nzGreen: "#32b956",
  nzGreenDark: "#00a82d",
  nzGreenDarker: "#00641B",

  nzBlueLighter: "#9999cd",
  nzBlueLight: "#6666b5",
  nzBlue: "#32329c",
  nzBlueDark: "#161699",
  nzBlueDarker: "#10106C",

  nzCyanLighter: "#97eee7",
  nzCyanLight: "#82eae2",
  nzCyan: "#2ac6bb",
  nzCyanDark: "#25b0a6",
  nzCyanDarker: "#0F6B6A",

  nzMagentaLighter: "#f9b2dc",
  nzMagentaLight: "#f57fc5",
  nzMagenta: "#f14cad",
  nzMagentaDark: "#ed1996",
  nzMagentaDarker: "#BC006F",

  /* Grays */
  bgGray: "#F7F7F7",
  white: "#fff",
  grayLightest: "#F0F0F0",
  grayLighter: "#b2b2b2",
  grayLight: "#999999",
  gray: "#7f7f7f",
  grayDark: "#4c4c4c",
  grayDarker: "#323232",
  grayDarkest: "#262328",
  black: "#181818",

  /* Other colors */
  darkSeaGreen: "#22504F",
  lightSeaGreen: "#55AEA6",
  dataExplanation: "#fdf7e2",
  errorRed: "#FF3025",
  errorRedPastel: "#FC6E76",
  blue: '#4bb7e2',
  gold: '#faaf40',
};

export const type = {
  fontSizeRoot: "14px",
  fontSizeBase: "1rem",
  lineHeightBase: "1.5",
  letterSpacingBase: "0",
  fontFamily: '"Gotham SSm A", "Gotham SSm B", sans-serif, Arial, Verdana',
  fontSize: [
    "0.75rem",
    "0.875rem",
    "1rem",
    "1.15rem",
    "1.5rem",
    "2rem",
    "2.5rem",
    "3rem",
    "3.5rem",
    "4rem",
    "4.5rem",
    "5rem",
  ],
  fontWeight: {
    thin: 300,
    book: 400,
    medium: 500,
    bold: 700
  }
};

export const curves = {
  standard: "cubic-bezier(0.4, 0.0, 0.2, 1)", // natural easing in and out
  decelerate: "cubic-bezier(0.4, 0.0, 1, 1)", // for elements entering the screen and slowing to a halt
  accelerate: "cubic-bezier(0.0, 0.0, 0.2, 1)", // for elements leaving the screen at full speed
  sharp: "cubic-bezier(0.4, 0.0, 0.6, 1)", // for elements leaving the screen that may return at any time
  bouncy: "cubic-bezier(0.68, -0.55, 0.265, 1.55)",
  easeInOutCubic: "cubic-bezier(0.645, 0.045, 0.355, 1)"
};

export const shadow = {
  primary: `0 2px 10px 1px rgba(0,0,0,0.15)`
}

export const transitions = {
  sm: "0.3s"
}

export const z = {
  deepDive: -9999,
  sub: -1,
  base: 100,
  loadingOverlay: 600,
  fixedCell: 700,
  triangle: 900,
  sticky: 1300,
  searchOverlay: 1350,
  searchbar: 1380,
  dropdown: 1400,
  popover: 1420,
  subnav: 1450,
  nav: 1500,
  modalBg: 1600,
  modal: 1700,
  tooltip: 1800,
  toaster: 2000,
  super: 9999
};

const theme = {
  breakpointsMin,
  breakpointsMax,
  containers,
  space,
  spacer,
  type,
  colors,
  curves,
  transitions,
  z,
  shadow
};

export default theme;
