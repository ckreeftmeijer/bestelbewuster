import { tint as polishedTint, shade as polishedShade } from 'polished';
import { css } from 'styled-components';
import theme from './theme';

export function isValidColor(color) {
  var ele = document.createElement('div');
  ele.style.color = color;
  return Boolean(
    ele.style.color
      .split(/\s+/)
      .join('')
      .toLowerCase(),
  );
}

export function shade(amount, color) {
  if (!color || !isValidColor(color)) return;
  if (typeof amount === 'string' && isValidColor(amount)) {
    if (isValidColor(amount)) {
      return polishedShade(1 - color, amount);
    }
    return amount;
  }
  if (isValidColor(color)) {
    return polishedShade(1 - amount, color);
  }
  return color;
}

export function tint(amount, color) {
  if (!color || !isValidColor(color)) return;
  if (typeof amount === 'string') {
    if (isValidColor(amount) && isValidColor(amount)) {
      return polishedTint(1 - color, amount);
    }
    return amount;
  }
  if (isValidColor(color)) {
    return polishedTint(1 - amount, color);
  }
  return color;
}

export function padding(side, amount) {
  let useEms = false;
  if (typeof side === 'number' || side.indexOf('em') > -1) amount = side;
  if (typeof amount === 'string' && amount.indexOf('em') > -1) {
    amount = amount.split('em')[0];
    useEms = true;
  }
  if (typeof amount !== 'number') return;
  const { spacer } = theme;
  if (side === 'top' || side === 't') {
    return css`
      padding-top: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'right' || side === 'r') {
    return css`
      padding-right: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'bottom' || side === 'b') {
    return css`
      padding-bottom: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'left' || side === 'l') {
    return css`
      padding-left: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'horizontal' || side === 'x') {
    return css`
      padding-left: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
      padding-right: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'vertical' || side === 'y') {
    return css`
      padding-top: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
      padding-bottom: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else {
    return css`
      padding: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  }
}

export function margin(side, amount) {
  let useEms = false;
  if (typeof side === 'number' || side.indexOf('em') > -1) amount = side;
  if (typeof amount === 'string' && amount.indexOf('em') > -1) {
    amount = amount.split('em')[0];
    useEms = true;
  }
  if (typeof amount !== 'number') return;
  const { spacer } = theme;
  if (side === 'top' || side === 't') {
    return css`
      margin-top: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'right' || side === 'r') {
    return css`
      margin-right: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'bottom' || side === 'b') {
    return css`
      margin-bottom: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'left' || side === 'l') {
    return css`
      margin-left: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'horizontal' || side === 'x') {
    return css`
      margin-left: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
      margin-right: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else if (side === 'vertical' || side === 'y') {
    return css`
      margin-top: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
      margin-bottom: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  } else {
    return css`
      margin: ${useEms ? `${amount}em` : `calc(${amount} * ${spacer})`};
    `;
  }
}

export function pos(side, amount) {
  const spacer = theme.spacer;
  if (side === 'top' || side === 't') {
    return css`
      top: calc(${amount} * ${spacer});
    `;
  } else if (side === 'right' || side === 'r') {
    return css`
      right: calc(${amount} * ${spacer});
    `;
  } else if (side === 'bottom' || side === 'b') {
    return css`
      bottom: calc(${amount} * ${spacer});
    `;
  } else if (side === 'left' || side === 'l') {
    return css`
      left: calc(${amount} * ${spacer});
    `;
  }
}

export function align(alignment) {
  switch (alignment) {
    case 'left':
      return css`
        text-align: left;
        margin-left: 0;
        left: 0;
      `;
    case 'center':
      return css`
        text-align: center;
        margin: 0 auto;
      `;
    case 'right':
      return css`
        text-align: right;
        margin-left: 0;
        right: 0;
      `;
    case 'justify':
      return css`
        text-align: justify;
      `;
    default:
      break;
  }
}

export function themify(color) {
  if (theme.colors[color]) {
    return theme.colors[color];
  }
  return color;
}

export function targetIE(version) {
  if (version >= 10) {
    return (...rules) => css`
      @media screen and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        ${css(...rules)};
      }
    `;
  } else if (version === 6 || version === 7 || version === 8) {
    return (...rules) => css`
      @media screen\9 {
        ${css(...rules)};
      }
    `;
  } else if (version === 8 || version === 9) {
    return (...rules) => css`
      @media screen\0 {
        ${css(...rules)};
      }
    `;
  } else if (!version) {
    return (...rules) => css`
      @media screen\9 {
        ${css(...rules)};
      }

      @media screen\0 {
        ${css(...rules)};
      }

      @media screen and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        ${css(...rules)};
      }
    `;
  }
}

const mixins = {
  shade,
  tint,
  padding,
  margin,
  align,
  isValidColor,
  themify,
  targetIE,
};

export default mixins;
