import React, { Component, Suspense } from 'react';
import { Route, HashRouter as Router } from "react-router-dom";

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey'

import HomePage from  './containers/HomePage';
import CreateEvent from  './containers/CreateEvent';
import FAQ from  './containers/FAQ';
import Header from  './components/Header';
import Footer from  './components/Footer';
import ScrollToTop from  './components/ScrollToTop';

import DayTrips from  './containers/DayTrips';
import BusinessCatering from  './containers/BusinessCatering';
import CoWorking from  './containers/CoWorking';
import Hotels from  './containers/Hotels';
import MKB from  './containers/MKB';
import BusinessEvents from  './containers/BusinessEvents';

import './theme/master.scss';
import './App.scss';

import './i18n'

const theme = createMuiTheme({
  palette: {
    primary: grey,
  },
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      headerWhite: false,
      windowWidth: window.innerWidth,
      navOpen: false,
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize.bind(this));
    window.addEventListener('scroll', this.listenScrollEvent);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize.bind(this));
  }

  handleResize = () => {
    this.setState({ windowWidth: window.innerWidth });
  }

  listenScrollEvent = e => {
    if (window.scrollY > 30) {
      this.setState({headerWhite: true})
    } else {
      this.setState({headerWhite: false })
    }
  }

  render() {
    const {  headerWhite, navOpen, windowWidth } = this.state;
    const mobile = windowWidth < 768;
    return (
      <Router>
        <ScrollToTop />
        <MuiThemeProvider theme={theme}>
          <Suspense fallback={null}>
            <div className="App">
              <Header
                headerWhite={headerWhite}
                navOpen={navOpen}
                mobile={mobile}
                toggleNav={() => this.setState({ navOpen: !navOpen })}
              />
                <Route path="/oplossingen/zakelijke-evenementen" component={BusinessEvents} />
                <Route path="/oplossingen/nieuwe-normaal" component={BusinessCatering} />
                <Route path="/segmenten/co-workspace" component={CoWorking} />
                <Route path="/segmenten/leisureparken" component={DayTrips} />
                <Route path="/segmenten/mkb" component={MKB} />
                <Route path="/segmenten/hotels" component={Hotels} />
                <Route path="/meeting-aanmaken" component={CreateEvent} />
                <Route path="/faq" component={FAQ} />
                <Route path="/" exact component={HomePage} mobile={mobile} />
              <Footer mobile={mobile} />
            </div>
          </Suspense>
        </MuiThemeProvider>
      </Router>
    );
  }
}

export default App;
