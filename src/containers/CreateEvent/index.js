import React from 'react';
import axios from "axios";
import { withTranslation } from 'react-i18next'
import Privacy from '../../files/Privacy_Policy_BestelBewuster_B.V._2019.pdf'

import Loader from '../../components/Loader'

import './styles.scss';

export class CreateEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailInput: '',
      formSent: false,
      error: undefined,
      loading: false,
    }
    this.createEvent = this.createEvent.bind(this);
  }
  createEvent() {
    const { t } = this.props;
    if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.emailInput)) {
      return alert(t('try.alert'))
    }
    this.setState({ loading: true })
    axios.post('https://api.bestelbewuster.nl/events/demo', {
      email: this.state.emailInput
    })
    .then((response) => {
      if (response.status === 200) {
        this.setState({ formSent: true, error: undefined, loading: false })
      } else {
        this.setState({ error: t('try.error'), loading: false })
      }
    })
    .catch((error) => {
      this.setState({ error: t('try.error'), loading: false})
    });
    this.setState({ emailInput: ''})
  }
  render() {
    const { emailInput, formSent, error, loading } = this.state;
    const { t } = this.props
    const steps = [
      t('try.steps.1'),
      t('try.steps.2'),
      t('try.steps.3'),
      t('try.steps.4'),
      t('try.steps.5'),
    ]
    return (
      <div className="create-event">
        <div className="create-event__container">
          <h1>{t('try.h1')}</h1>
          <p className="text-center">{t('try.p')}</p>
          <div className="steps-container">
          {steps.map((step, index) => (
            <div className="step-container">
              <div className="step-number">{index + 1}</div>
              <div className="step-text">{step}</div>
            </div>
          ))}
          </div>
          <div>
            <input
             id="email"
             placeholder="Email"
             value={emailInput}
             onChange={(e) => this.setState({ emailInput: e.target.value })}
             margin="normal"
             autocomplete="false"
            />
            <div className="input-bottom" />
          </div>
          <div className="event-submit" onClick={() => this.createEvent()}>
            {loading
              ? <Loader className="event-loader" />
              : formSent ? t('try.sent') : t('try.create')
            }
          </div>
          <div className="small">Door een event aan te maken ga je akkoord met onze <a className="white" href={Privacy} download>{t('privacy')}</a></div>
          <div className="error-message">{error}</div>
          <br />
          <div>{t('try.sub')}</div>
        </div>
      </div>
    )
  }
}


export default withTranslation()(CreateEvent)
