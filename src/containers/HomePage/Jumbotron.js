import React from 'react'

import styled from 'styled-components'

import BackgroundS from './background_small.svg'
import Container from '../../components/Container'
import Basic from '../../components/Hubspot/Basic'

import media from '../../theme/styledComponents/breakpoints';

const Jumbotron = () => (
  <Wrapper>
    <Container>
      <Content>
        <h5 className="gray-light margin-bottom">Voorkom voedselverspilling</h5>
        <h2 className="gray bold" style={{ lineHeight: '50px'}}> BestelBewuster faciliteert een bewuste en persoonlijke keuze bij cateringmomenten</h2>
      </Content>
      <CardContainer>
        <Basic page="homepage" />
      </CardContainer>
    </Container>
  </Wrapper>
)

const Wrapper = styled.div`
  background: url(${BackgroundS});
  background-size: cover;
  padding: 30px;
  min-height: 600px;
`

const Content = styled.div`
  padding-top: 150px;
  vertical-align: top;

  ${media.lg`
    display: inline-block;
    width: 45%;
  `};
`

const CardContainer = styled.div`
  padding-top: 120px;
  position: relative;
  top: -20px;
  vertical-align: top;

  ${media.lg`
    padding: 0px 10%;
    padding-top: 120px;
    width: 50%;
    display: inline-block;
  `};
`



export default Jumbotron
