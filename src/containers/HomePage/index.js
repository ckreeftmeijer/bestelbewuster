import React, { Component } from "react";
import TagManager from 'react-gtm-module'
import { Link } from "react-router-dom";
import { withTranslation } from 'react-i18next'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import HubspotService from '../../services/HubspotService';

import "./styles.scss";

import Steps from '../../components/Steps'
import Pricing from '../../components/Pricing'
import Container from '../../components/Container'
import Jumbotron from './Jumbotron'
import Slider from '../../components/CardSlider/PageWide'
import SliderCard from '../../components/CardSlider/SliderCard'
import Spacer from '../../components/Spacer'


import DenHaagLogo from "../../images/den_haag_logo.jpg";
import SDG9_NL from "../../images/SDG/SDG-icon-NL-RGB-09.jpg";
import SDG11_NL from "../../images/SDG/SDG-icon-NL-RGB-11.jpg";
import SDG12_NL from "../../images/SDG/SDG-icon-NL-RGB-12.jpg";
import SDG13_NL from "../../images/SDG/SDG-icon-NL-RGB-13.jpg";
import SDG9_EN from "../../images/SDG/E-WEB-Goal-09.png";
import SDG11_EN from "../../images/SDG/E-WEB-Goal-11.png";
import SDG12_EN from "../../images/SDG/E-WEB-Goal-12.png";
import SDG13_EN from "../../images/SDG/E-WEB-Goal-13.png";

import Amsterdam from "../../images/SIR/amsterdam.png";
import Sir from "../../images/SIR/logo-text.png";

import IconData from "../../images/icon_grapgh.png";
import IconFoodwaste from "../../images/icon_foodwaste.png";
import IconMeeting from "../../images/icon_meeting.png";


const hubspot = new HubspotService()


class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      contactInput: "",
      contactRequestSent: false,
      cookieMessage: false,
      acceptedTerms: false,
    };
    this.sendContactDetails = this.sendContactDetails.bind(this);
  }

  componentDidMount() {
    const cookieApproved = localStorage.getItem("cookies_approved");
    setTimeout(() => {
      this.setState({ cookieMessage: !Boolean(cookieApproved) })
    }, 1000)

  }
  sendContactDetails(email) {
    const body = {
      email
    };
    fetch(
      "https://api.bestelbewuster.nl/contact-form",
      {
        method: "post",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json"
        }
      }
    ).then(res => {
      if (res.status === 200) {
        this.setState({ contactRequestSent: true });
      }
    });
    this.setState({ contactInput: "" });
  }
  sendFormRequest = async (email) => {
    if (!this.state.acceptedTerms) {
      return toast(this.props.t('more.acceptWarning'), {
          type: 'warning',
          autoClose: 5000,
        });
    }
    hubspot.submitForm({
      formId: '88fc3747-1768-43a4-989f-0252ae30eaff',
      fields: [
        {
          name: 'email',
          value: email
        },
      ],
      comunications: true
    })
    .then(res => {
      this.setState({ contactRequestSent: true });
      toast(this.props.t('more.accepted'), {
        type: 'success',
        autoClose: 5000,
      });
      this.setState({ contactInput: "" });
    })
    .catch(err => {})
  }
  closeCookie(choice) {
    this.setState({ cookieMessage: false })
    localStorage.setItem("cookies_approved", choice);
  }
  render() {
    const { contactInput, contactRequestSent, cookieMessage, acceptedTerms } = this.state;
    const { t, i18n, history } = this.props;
    const currentLang = i18n.language;
    const steps = [
      t('process.step.1'),
      t('process.step.2'),
      t('process.step.3'),
      t('process.step.4'),
      t('process.step.5'),
      t('process.step.6'),
      t('process.step.7'),
    ]

    const cards = [
      { letter: "C", link: "/segmenten/co-workspace", title: "Co-workspace", text: "Service naar alle member zonder verspilling"},
      { letter: "H", link: "/segmenten/hotels", title: "Hotels", text: "Digitalisering van het ontbijtbuffet zonder verspilling"},
      { letter: "L", link: "/segmenten/leisureparken", title: "Leisure", text: "Het meest bewuste dagje uit voor de bezoekers"},
      { letter: "M", link: "/segmenten/mkb", title: "MKB", text: "Efficiente boodschappenlijst naar wens van alle medewerkers"},
      { letter: "N", link: "/oplossingen/nieuwe-normaal", title: "Nieuwe normaal", text: "De catering veilig en digitaal"},
      { letter: "Z", link: "/oplossingen/zakelijke-evenementen", title: "Zakelijke evenementen", text: "Persoonlijke catering op alle evenementen"},
    ]

    return (
      <div className="App homepage">
        <ToastContainer />
        <div className={`${cookieMessage ? 'homepage__overlay' : ''}`} />
        <Jumbotron />
        <div className="home-container" style={{ marginTop: '30px'}}>
          <div className="usps">
            <div className="usp">
              <div className="usp-title">
                <img src={IconFoodwaste} className="usp-icon" alt="usp icon" />
              </div>
              <div className="usp-subtitle">
                <div>{t('usp.1.title')}</div>
                <span>{t('usp.1.text')}</span>
              </div>
            </div>
            <div className="usp">
              <div className="usp-title">
                <img src={IconMeeting} className="usp-icon" alt="usp icon" />
              </div>
              <div className="usp-subtitle">
              <div>{t('usp.2.title')}</div>
              <span>{t('usp.2.text')}</span>
              </div>
            </div>
            <div className="usp">
              <div className="usp-title">
                <img src={IconData} className="usp-icon" alt="usp icon" />
              </div>
              <div className="usp-subtitle">
                <div>{t('usp.3.title')}</div>
                <span>{t('usp.3.text')}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="color-container problem-container">
          <div className="color-container-content">
            <div className="h1">{t('why.problem')}</div>
            <p>{t('why.p1')}</p>
            <p>{t('why.p2')}</p>
            <div className="container">
              <div className="problem-side col-12 col-md-6">
                <div className="font-size-md">{t('why.problem')}</div>
                <span>{t('why.problem.1')}</span>
                <span>{t('why.problem.2')}</span>
                <span>{t('why.problem.3')}</span>
              </div>
              <div className="problem-side col-12 col-md-6">
                <div className="font-size-md">{t('why.solution')}</div>
                <span>{t('why.solution.1')}</span>
                <span>{t('why.solution.2')}</span>
                <span>{t('why.solution.3')}</span>
              </div>
            </div>
            <br /><br />
            <p>{t('why.p3')}</p>
            <p>{t('why.p4')}</p>
          </div>
        </div>

        <div className="home-container">
        <div
          className="video"
          style={{
            position: "relative",
            paddingBottom: "56.25%" /* 16:9 */,
            paddingTop: 25,
            height: 0
          }}
          >
          <iframe
            title="BestelBewuster introductie film"
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              width: "100%",
              height: "100%"
            }}
            src={`
              https://www.youtube.com/embed/v_aaMGp_33M`}
            frameBorder="0"
          />
          </div>
        </div>

        <Container>
          <Slider>
            {cards.map((card, i) => (
              <SliderCard
                key={i}
                letter={card.letter}
                title={card.title}
                text={card.text}
                handleClick={() => history.push(card.link)}
                index={1}
              />
            ))}
          </Slider>
        </Container>


        <div className="color-container container">
          <div className="col-12 col-md-6 pro-side">
            <div className="bb-pros">
            <div className="font-size-half bold">{t('adv')}</div>
              <ul>
                <li>{t('adv.1')}</li>
                <li>{t('adv.2')}</li>
                <li>{t('adv.3')}</li>
                <li>{t('adv.4')}</li>
                <li>{t('adv.5')}</li>
                <li>{t('adv.6')}</li>
                <li>{t('adv.7')}</li>
              </ul>
            </div>
          </div>
          <div className="col-12 col-md-6 pro-side">
            <div className="font-size-half bold white">{t('adv.try')}</div>
            <p className="color-container__text">
              {t('adv.text')}
            </p>
            <Link to="/meeting-aanmaken">
              <div className="yellow-button">{t('adv.cta')}</div>
            </Link>
          </div>
        </div>

        <Spacer amount={28} vertical />

        <div className="home-container">
          <Steps
            steps={steps}
          />
        </div>

        <Spacer amount={28} vertical />

        <Container>
          <Pricing />
        </Container>

        <Spacer amount={28} vertical />

        <div className="home-container container">
          <div className="col-12 col-md-6">
            <div className="sdg-container">
              <img src={currentLang === 'nl' ? SDG9_NL : SDG9_EN} alt="SDG icoon" />
              <img src={currentLang === 'nl' ? SDG11_NL : SDG11_EN} alt="SDG icoon" />
              <img src={currentLang === 'nl' ? SDG12_NL : SDG12_EN} alt="SDG icoon" />
              <img src={currentLang === 'nl' ? SDG13_NL : SDG13_EN} alt="SDG icoon" />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="sdg-text">
              <div>{t('tech.title')}</div>
              <p>{t('tech.p1')}</p>
              <p>{t('tech.p2')}</p>
            </div>
          </div>
        </div>

        <br />
        <div className="more-info">
          <div className="home-container">
            <div className="green">
             {t('more.t1')}  <br/> {t('more.t2')}
            </div>
            <div className="contact">
              <input
                value={contactInput}
                placeholder="email"
                type="text"
                onChange={e => this.setState({ contactInput: e.target.value })}
              />
              <div
                className="contact-button"
                onClick={() => this.sendFormRequest(contactInput)}
                style={{
                  background: `${
                    contactRequestSent
                      ? "#1e8a7b"
                      : "linear-gradient(45deg, #1e8a7b, #f5ed7f)"
                  }`
                }}
              >
                {contactRequestSent ? t('more.sent') : t('more.signup')}
              </div>
              <div className="contact__checkbox-container">
                <input checked={acceptedTerms} type="checkbox" onClick={() => this.setState({ acceptedTerms: !acceptedTerms})} />
                <label>Ik geef toestemming communicatie van BestelBewuster te ontvangen</label>
              </div>
            </div>
          </div>
        </div>
        <div className="home-container">
          <div>
            <div className="third">
              <img
                className="the-hague-logo"
                src={DenHaagLogo}
                alt="logo gemeente Den Haag"
              />
            </div>
            <div className="twothird">
              {t('theHague.quote')}
            </div>
          </div>
                    <div className="divider" />
          <div className="awards">
            <div className="half text-center">
              <img className="award-image" src={Sir} alt="award icon" />
              <div className="award-title">
                {t('win.theHague.title')}
              </div>
              {t('win.theHague.subTitle')}
            </div>
            <div className="half text-center">
              <img className="award-image" src={Amsterdam} alt="award icon" />
              <div className="award-title">
                {t('win.amsterdam.title')}
              </div>
              {t('win.amsterdam.subTitle')}
            </div>
          </div>
        </div>
        <div className={`homepage__cookie ${cookieMessage ? 'homepage__cookie--active' : ''}`}>
          {t('cookie')}
          <div className="homepage__buttons">
            <div onClick={() => this.closeCookie('approved')} className="cookie-button">{t('cookie.b1')}</div>
            <div onClick={() => this.closeCookie('tracking')} className="cookie-button">{t('cookie.b2')}</div>
            <div onClick={() => this.closeCookie('not approved')} className="cookie-button">{t('cookie.b3')}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(HomePage);
