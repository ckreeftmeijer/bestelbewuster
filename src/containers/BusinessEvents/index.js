import React from 'react'

import CTA from '../../components/CTA'
import Trinity from '../../components/Trinity'
import FaqComponent from '../../components/FaqComponent'
import ContainerWithImage from '../../components/ContainerWithImage'
import FromPricing from '../../components/FromPricing'
import Intro from '../../components/Intro'
import Container from '../../components/Container'
import Card from '../../components/Card'
import Spacer from '../../components/Spacer'
import SimpleSteps from '../../components/SimpleSteps'
import SliderCard from '../../components/CardSlider/SliderCard'

import Advantages from '../../images/advantages.png'

const BusinessCatering = ({ history }) => {
  return (
    <div className="page-container">
      <Intro
        title="ZAKELIJKE EVENEMENTEN"
        form="zakelijke evenementen"
      >
        <div>Veel bedrijven, organisaties en evenementenlocaties hebben een slecht inzicht in hoeveel gasten er zullen komen en wat er precies gewenst is qua eten bij het evenement. Hierdoor wordt er onnodig veel voedsel weggegooid en staat er vaak te veel personeel.</div>
        <br />
        <div>BestelBewuster software zorgt voor extra inzicht in opkomst en in dieetwensen. Daarnaast wordt het aanbod bewuster en de inkoop duurzamer.  </div>
      </Intro>
      <CTA
        inverted
        title="Minder verspilling in voedsel, duurzame inkoop en efficiëntere personeelsbezetting"
        href="mailto:info@bestelbewuster.nl"
      />
      <Container>
        <Trinity>
          {[
            { title: 'De juiste producten voor de juiste doelgroep', text: 'Steeds meer consumenten maken de overstap naar vegetarisch of zelfs veganistisch voedsel. Hierdoor lopen de wensen van gasten steeds verder uiteen. Hoe prettig zou het zijn als je precies zou weten wat de klanten zouden willen eten? Een tevreden consument en geen verspilling? Via de BestelBewuster software maken wij deze wensen vooraf inzichtelijk.', },
            { title: 'Nooit meer teveel personeel tijdens evenementen ', text: 'Via de BestelBewuster software geeft de consument niet alleen aan wat zijn dieetwensen zijn, maar moet hij/zij ook aangeven of hij daadwerkelijk aanwezig is. Via onze software krijg je dus beter inzichtelijk hoeveel gasten er komen en hoeveel personeel daarvoor nodig is. Maak dus geen onnodige kosten door te veel personeel in te zetten! ', },
            { title: 'Wordt koploper in verspilling voorkomen tijdens evenementen', text: 'Laat je gasten zien dat het voorkomen van verspilling belangrijk is voor jullie bedrijf. Laat de gast daarnaast bewuste keuzes maken door aan te geven wat de impact is van bepaalde gerechten op het milieu.', },
          ]}
        </Trinity>
        <Spacer amount={18} vertical />
        <div className="green h3">De oplossing: Netwerken zonder verspilling</div>
        <Spacer amount={8} vertical />
        <div className="container margin--lg">
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={1}
              title="Voordelen voor bezoekers"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Bewuster kiezen</li>
                  <li>Digitaal menuaanbod en beleving van restaurant</li>
                  <li>Niet teleurgesteld worden dat er geen passend gerecht beschikbaar is</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={2}
              title="Voordelen voor de organisator"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Inkoop en personeel management verbeteren</li>
                  <li>Verspilling verminderen</li>
                  <li>Omzet verhogen</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={3}
              title="Voordelen voor de cateraar"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Inkoopmanagement verbeteren</li>
                  <li>Omzet verhogen</li>
                  <li>Bezoekersstromen reguleren</li>
                </ul>
              </div>
            </Card>
          </div>
        </div>
      </Container>
      <Spacer amount={20} vertical />
      <Container>
        <div className="green h3">Hoe het werkt</div>
        <Spacer amount={7} vertical />
        <SimpleSteps
          steps={[
            {title: 'Communicatie kanalen', text: 'Bedrijf of particulier meld zich aan voor een evenement/congres of bijeenkomst.'},
            {title: 'Bewuste voorkeuren', text: 'Bedrijf of particulier levert genodigden lijst aan'},
            {title: 'Reward / incentive', text: 'Genodigden lijst wordt ingeladen in de BestelBewuster software'},
            {title: 'Consumeren', text: 'Genodigden krijgen een e-mail of whatsapp om aan te geven of ze aanwezig zijn en wat ze willen eten'},
            {title: 'Analyse', text: 'Organisatie ontvangt lijst met aanwezigen en dieet wensen'},
          ]}
        />
      </Container>
      <Spacer amount={8} vertical />
      <CTA
        inverted
        title="Ook als evenementen organisator verspilling verminderen en kosten besparen? Neem nu contact op!"
        href="mailto:info@bestelbewuster.nl"
      />
      <Spacer amount={10} vertical />
      <ContainerWithImage image={Advantages}>
        <>
          <div className="green h3">De unieke voordelen</div>
          <div className="pro-side no-padding">
          <ul>
            <li>Huisstijl volledig inregelbaar </li>
            <li>Digitalisering catering</li>
            <li>Data en inzichten via dashboard</li>
            <li>Plug and play software</li>
            <li>Review mogelijkheden</li>
            <li>Privacy en security safe</li>
          </ul>
          </div>
        </>
      </ContainerWithImage>
      <Spacer amount={8} vertical />
      <FromPricing
        inverted
        title="Vanaf € 50,- per maand"
        href="mailto:info@bestelbewuster.nl"
      />
      <div>
        <div style={{ maxWidth: '500px', margin: '0 auto', cursor: 'pointer' }}>
          <Spacer amount={8} vertical />
          <Card>
            <SliderCard
              title="Corona"
              handleClick={() => history.push("/oplossingen/nieuwe-normaal")}
              letter={'C'}
              index={1}
            >
            Organiseer jij zakelijke evenementen waarbij je rekening wilt houden wilt houden met de 1,5 meter samenleving? Check ook onze nieuwe normaal pagina voor meer info.
            </SliderCard>
          </Card>
        </div>
      </div>
      <Spacer amount={10} vertical />
      <FaqComponent
        items={[
          {
            title: 'Worden de gegevens van bezoekers opgeslagen?',
            content: 'Nee, de software van BestelBewuster is volledig privacy en security veilig. Gegevens worden direct encrypted en nadat het bezoek is geweest verwijderd.'
          },
          {
            title: 'Moeten evenement bezoekers verplicht een lunch keuze opgeven?',
            content: 'Nee, de systemen staan los van elkaar. Een bezoeker wordt vanuit de verspilling gedachte gevraagd of die van te voren een keuze wil opgeven voor bijvoorbeeld lunch en/of ontbijt. Dit is geen verplichting maar een vraag. De vraag wordt gesteld via een aparte mail of er staat een link via bijvoorbeeld de agenda uitnodiging. '
          },
          {
            title: 'Hoe wordt verspilling voorkomen via jullie systeem?',
            content: 'BestelBewuster heeft algemene data over bezoek en consumptie. Dit vullen wij aan met bezoekersprofielen die wij maken per evenement. Dit profiel komt tot stand door het uitvragen van voorkeur voor catering. Dit kan worden voorspelt of worden uitgegaan van een verplichte bestelling.'
          },
          {
            title: 'Waarom zou een bezoeker van te voren hieraan meewerken?',
            content: 'Duurzaamheid wordt met de dag belangrijker. Zowel voor bedrijven als consumenten. Door dit te communiceren zijn de eerste bezoekers bereid om mee te werken. Daarnaast kan er bijvoorbeeld gewerkt worden met een beloningssysteem. Denk aan een kortingsvoucher die je uitgeeft voor het voorkeur restaurant aangegeven door de bezoeker. Daarmee voorkom je niet alleen verspilling, maar verhoog je ook omzet.'
          },
        ]}
      />
    </div>
  )
}

export default BusinessCatering
