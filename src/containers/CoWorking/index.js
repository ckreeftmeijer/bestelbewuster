import React from 'react'

import CTA from '../../components/CTA'
import Trinity from '../../components/Trinity'
import FaqComponent from '../../components/FaqComponent'
import ContainerWithImage from '../../components/ContainerWithImage'
import FromPricing from '../../components/FromPricing'
import Intro from '../../components/Intro'
import Container from '../../components/Container'
import Card from '../../components/Card'
import Spacer from '../../components/Spacer'
import SimpleSteps from '../../components/SimpleSteps'
import SliderCard from '../../components/CardSlider/SliderCard'

import Advantages from '../../images/advantages.png'

const CoWorking = ({ history }) => {
  return (
    <div className="page-container">
      <Intro
        title="CO-WORKSPACE"
        form="co-working"
      >
        <div>Catering in co-working, co-workspaces en verzamelgebouwen is een van de essentiële services van de organisaties. Een van de belangrijkste redenen om onderdeel te worden van een coworking space is om mensen te ontmoeten, van een goede zakelijke lunch te kunnen genieten of even te ontspannen.</div>
        <br/>
        <div>
          BestelBewuster software zorgt voor extra omzet terwijl verspilling wordt voorkomen. Daarnaast wordt het aanbod bewuster, persoonlijker en de inkoop duurzamer.
        </div>
      </Intro>
      <CTA
        inverted
        title="Bewust en vers aanbod vergroten zonder te verspillen en direct meer omzet"
        href="mailto:info@bestelbewuster.nl"
      />
      <Container>
        <Trinity>
          {[
            { title: 'Persoonlijker en bewuster aanbod', text: 'Al de members, bedrijven en bezoekers van je co-workspace zijn je klanten en ambassadeurs. Iedereen is anders en dus wil je de catering zo persoonlijk mogelijk aanbieden om iedereen zo goed mogelijk te bedienen. Met BestelBewuster creëren we een extra digitaal contactmoment, waar iedereen zijn of haar persoonlijke bewuste voorkeur vooraf kan aangeven.', },
            { title: 'Minder verspilling', text: 'Het aanbieden van vers voedsel gaat al snel gepaard met meer voedselverspilling. Door meer inzicht vooraf op specifiek niveau, het behalen van inzichten uit de historische data kan de inkoop beter worden ingeschat en dus minder verspilling optreed.', },
            { title: 'Omzet verhogen via efficiënter inkoopbeleid', text: 'De members kunnen vooraf op week, dag van te voren of op dezelfde dag nog hun bewuste bestellingen doen waardoor er beter kan worden ingekocht. Dit zorgt voor een omzetvergroting en/of een betere cashpositie omdat ze niet op de dag zelf ergens anders gaan lunchen. Daarnaast kan er worden gestuurd op marge in de presentatie van de banquetingmap, en up-en cross selling.', },
          ]}
        </Trinity>
        <Spacer amount={18} vertical />
        <div className="green h3">De oplossing: Minder verspilling en meer omzet bij catering binnen de co-workspace</div>
        <Spacer amount={8} vertical />
        <div className="container margin--lg">
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={1}
              title="Voordelen voor members"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Bewuster kiezen</li>
                  <li>Bijdragen aan minder verspilling</li>
                  <li>Tijdswinst, nooit meer wachten in de rij</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={2}
              title="Voordelen voor de co-workspace"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Bewust en duurzame uitstraling</li>
                  <li>Plek om te ontmoeten nog beter faciliteren</li>
                  <li>Uitstraling naar externe bezoekers</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={3}
              title="Voordelen voor de cateraar"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Minder voedselverspilling & inkoopmanagement verbeteren</li>
                  <li>Omzet verhogen & cashpositie verbeteren</li>
                  <li>Meer data en inzichten over consumptie</li>
                </ul>
              </div>
            </Card>
          </div>
        </div>
      </Container>
      <Spacer amount={20} vertical />
      <Container>
        <div className="green h3">Hoe het werkt voor de bezoeker</div>
        <Spacer amount={7} vertical />
        <SimpleSteps
          steps={[
            {title: 'Communicatie kanalen', text: 'Member kan via link, mail of direct online zijn cateringvoorkeuren aangeven'},
            {title: 'Bewuste voorkeuren', text: 'Aan de hand van het dagelijkse menu van de cateraar kan voor één of meer dagen vooruit besteld worden'},
            {title: 'Reward / incentive', text: 'Members kunnen optioneel online afrekenen, dit doen aan de kassa of op rekening'},
            {title: 'Consumeren', text: 'De bestellingen worden afgehaald door member bij restaurant, pick up point of krijgt deze aan het bureau bezorgd binnen de co-working'},
            {title: 'Analyse', text: 'Cateraar heeft via dashboard dagelijks inzicht ten behoeve van de optimale inkoop'},
          ]}
        />
      </Container>
      <Spacer amount={8} vertical />
      <CTA
        title="Geïnteresseerd?! Klik hier voor een afspraak!"
        href="mailto:info@bestelbewuster.nl"
        inverted
      />
      <Spacer amount={10} vertical />
      <ContainerWithImage image={Advantages}>
        <>
          <div className="green h3">De unieke voordelen</div>
          <div className="pro-side no-padding">
            <ul>
              <li>Huisstijl volledig inregelbaar </li>
              <li>Digitalisering inkoop</li>
              <li>Data en inzichten via dashboard</li>
              <li>Plug and play software</li>
              <li>Privacy en security safe</li>
            </ul>
          </div>
        </>
      </ContainerWithImage>
      <Spacer amount={8} vertical />
      <FromPricing
        inverted
        title="Vanaf € 50,- per maand"
        href="mailto:info@bestelbewuster.nl"
      />
      <div>
        <div style={{ maxWidth: '500px', margin: '0 auto', cursor: 'pointer' }}>
          <Spacer amount={8} vertical />
          <Card>
            <SliderCard
              title="Evenementen"
              handleClick={() => history.push("/oplossingen/zakelijke-evenementen")}
              letter={'E'}
              index={1}
            >
              Worden er evenementen georganiseerd op locatie? Zie ook de “evenementen” pagina
            </SliderCard>
          </Card>
        </div>
      </div>
      <Spacer amount={10} vertical />
      <FaqComponent
        items={[
          {
            title: 'Worden de gegevens van bezoekers opgeslagen?',
            content: 'Nee, de software van BestelBewuster is volledig privacy en security veilig. Gegevens worden direct encrypted en nadat het bezoek is geweest verwijderd.'
          },
          {
            title: 'Moeten members verplicht een lunch keuze opgeven?',
            content: 'Nee, de systemen staan los van elkaar. Een member wordt vanuit de verspilling gedachte gevraagd of die van te voren een keuze wil opgeven voor. Dit is geen verplichting maar een vraag. De vraag wordt gesteld via een aparte mail of er staat een link op bijvoorbeeld de kantoor sharepoint.'
          },
          {
            title: 'Hoe wordt verspilling voorkomen via jullie systeem?',
            content: 'BestelBewuster heeft algemene data over bezoek en consumptie. Dit vullen wij aan met bezoekersprofielen die wij maken op dagniveau. Dit profiel komt bijvoorbeeld tot stand door het uitvragen van bijvoorbeeld restaurant voorkeur bij bezoekers. Daarnaast wordt ook indirecte verspilling voorkomen. Het doel is om bezoekers te laten consumeren in het parken en daarmee eigen consumpties thuis laten. Mensen gooien veel “rugzak” artikelen aan het einde van de dag onverhoopt weg.'
          },
          {
            title: 'Waarom zou een members van te voren hieraan meewerken.',
            content: 'Duurzaamheid wordt met de dag belangrijker. Zowel voor bedrijven als consumenten. Door dit te communiceren zijn de medewerkers bereid om mee te werken. Zo voelt iedereen zich verantwoordelijk om een bewuste keuze te maken.'
          },
          {
            title: 'Waarom zou dit systeem omzet verhogend werken?',
            content: 'Als de members vooraf al hun bestelling voor bijvoorbeeld een week doen, wordt hiermee voorkomen dat ze op het moment zelf toch buiten een broodje gaan eten. Daarnaast kan er beter worden ingekocht en meer met versproducten worden gewerkt, die minder worden weggegooid omdat de inschatting voor de inkoop beter kan worden gedaan.'
          },
        ]}
      />
    </div>
  )
}

export default CoWorking
