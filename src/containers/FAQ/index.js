import React from 'react';
import { withTranslation } from 'react-i18next'

import Collapsable from '../../components/Collapsable'

import './styles.scss';

export class FAQ extends React.Component {
  state = {
    isOpen: undefined
  }
  render() {
    const { isOpen } = this.state
    const { t } = this.props;
    const items = [
      {
        title: 'Hoe gebruik ik BestelBewuster.nl voor een lunch meeting?',
        content: <ol>
          <li>Installeer de BestelBewuster plug-in in jouw agenda applicatie (outlook,gmail)</li>
          <li>Organiseer een meeting zoals je gewend bent en druk hierbij op het BestelBewuster icoon.</li>
          <li>BestelBewuster monitort of genodigden aanwezig zijn en stuurt hen een e-mail waarmee ze kunnen aangeven wat ze willen lunchen.</li>
          <li>BestelBewuster verzamelt een lijst met de bestelde producten en geeft deze door aan de organisator en/of cateraar zodat er niet verkeerd bestelt wordt.</li>
        </ol>,
      },
      {
        title: 'Ik heb een e-mail van BestelBewuster ontvangen wat nu?',
        content: 'Zodra je een e-mail van BestelBewuster.nl hebt ontvangen ben je uitgenodigd voor een lunch meeting. De organisatie waar de lunch zal plaatsvinden wil iets doen aan het voedselverspilling probleem. In de ontvangen e-mail kan je aangeven wat je graag zou willen eten zodat er nooit teveel of te weinig lunch geserveerd zal worden tijdens de meeting. ',
      },
      {
        title: 'Ik heb geen e-mail van BestelBewuster ontvangen, maar ben wel uitgenodigd door een aangesloten organisatie, wat nu?',
        content: 'Check als eerste je spambox. Vervolgens kan je nagaan of de organisator van de lunch meeting de koppeling met BestelBewuster.nl heeft gebruikt tijdens het aanmaken van de meeting. Als dat het geval is kan je altijd contact opnemen met support@bestelbewuster.nl.',
      },
      {
        title: 'Ik heb geen lunch voorkeur opgegeven en de invite mail verwijderd, maar wil toch graag mee-eten. Kan dat nog?',
        content: 'Ja dat kan zeker! Wanneer je geen voorkeur hebt opgegeven krijg je vanzelf 72 uur voor aanvang van de meeting een herinneringsmail om alsnog je lunchbestelling door te geven. ',
      },
      {
        title: 'Ik heb geen technische kennis. Is BestelBewuster eenvoudig te gebruiken?',
        content: 'BestelBewuster is zeer eenvoudig te gebruiken want het integreert naadloos in je eigen agenda applicatie. Om BestelBewuster bij jouw bedrijf werkend te krijgen zijn ICT integraties noodzakelijk. Om de plug-and-play software draaiende te krijgen is er een simpele checklist beschikbaar voor ieder bedrijf. Wil je meer weten? Vraag de checklist aan via info@bestelbewuster.nl.',
      },
      {
        title: 'Is BestelBewuster ook met Gmail en G-Suite te gebruiken?',
        content: 'De BestelBewuster applicatie is met alle e-mailclients te gebruiken.',
      },
      {
        title: 'Is BestelBewuster ook met Outlook en Office 365 te gebruiken?',
        content: 'De BestelBewuster applicatie is met alle e-mailclients te gebruiken.',
      },
      {
        title: 'Kan ik na mijn bestelling mijn keuze nog wijzigen?',
        content: 'Elk cateraar/bedrijf wil een aantal uur voor de meeting de definitieve bestelling weten. Tot dat moment kan een bestelling nog worden aangepast. Dit doe je via de ontvangen orderbevestiging e-mail of herinneringsmail die je een aantal dagen voor de lunch meeting ontvangt. ',
      },
      {
        title: 'Wat gebeurt er als ik mijn meeting annuleer?',
        content: 'Wanneer de lunch meeting geannuleerd wordt, vóór de deadline van de cateraar, zal automatisch de lunchbestelling geannuleerd worden. Zo voorkomen wij samen verspilling.',
      },
      {
        title: 'Kan ik de organisatie van een meeting overdragen?',
        content: 'Het is helaas nog niet mogelijk binnen BestelBewuster om de organisatie van een lunch meeting over te dragen aan iemand anders. Hiervoor dient een nieuw event aangemaakt te worden en zullen alle aanwezige opnieuw uitgenodigd moeten worden. ',
      },
      {
        title: 'Wat staat er op het menu van BestelBewuster?',
        content: 'Het lunch aanbod is afhankelijk van de gecontracteerde cateraar van de organisatie waar de lunch meeting zal plaatsvinden.',
      },
      {
        title: 'Als ik een meeting organiseer hoe kan ik dan aangeven wat er besteld kan worden?',
        content: 'Met  de plugin binnen jouw agenda kun je, als organisator, aangeven vanuit welk menu er voor de gebruikers besteld kan worden. De cateraar en/of het bedrijf bepalen de opties (de menukaart)',
      },
      {
        title: 'Hoe geef ik als organisator een lunchbestelling op?',
        content: 'De organisator ontvangt een bevestigingsmail dat de lunch meeting is georganiseerd. Via deze mail heeft hij ook de mogelijkheid om zijn of haar lunch voorkeur op te geven. ',
      },
      {
        title: 'Hoe weet een cateraar wat mijn bestelling is?',
        content: 'Een aantal uren/dagen voor de daadwerkelijke lunch meeting ontvangt de organisator de totale bestelling. Deze moet hij of zij doorzetten naar de cateraar. Indien gewenst kan er vanuit BestelBewuster een koppeling gemaakt worden met het facilitair management informatie systeem (FMIS) en/of bestelsysteem zodat de totale bestelling direct wordt ingeschoten bij het systeem van de cateraar.',
      },
      {
        title: 'Zijn er beperkingen/regels voor het bestelproces bij wie ik kan bestellen?',
        content: <><p>Speciale regels en uitzonderingen kunnen per bedrijf ingesteld worden. Het type lunch (bijvoorbeeld basis of luxe) kan een organisator zelf bepalen. Tevens kan de  organisator aangeven  wat voor soort lunch hij of zij aanmaakt.</p>
                <p>Bij wie je kan bestellen bepaalt een organisatie zelf. De menukaart kan van bijvoorbeeld de bestaande cateraar bij een bedrijf zijn maar ook de lokale leverancier om de hoek.</p></>
      },
      {
        title: 'Wat zijn de kosten van de dienst?',
        content: 'BestelBewuster is een software tool waar de pricing gebaseerd is op het aantal meetings dat een bedrijf organiseert. De tool verdient zichzelf terug uit de besparing die gerealiseerd wordt. Gemiddeld kost het maar 20% van wat je gaat besparen. Kortom, minder voedselverspilling en minder kosten.',
      },
      {
        title: 'Voor welke bedrijven is de tool geschikt?',
        content: 'In principe voor alle bedrijven die zakelijke lunches, koffie & thee meetings, ontbijt en/of evenementen organiseren. Over het algemeen werkt BestelBewuster met bedrijven samen vanaf 500 medewerkers of meer, dan wel bedrijven die grotere evenementen organiseren. De pricing is gebaseerd op meetings wat het interessant kan maken voor elk soort bedrijf. Met de eerste staffel tot 150 meetings en 1.500 invites kun je al voor € 399,- starten met het minderen van voedselverspilling. ',
      },
      {
        title: 'Vraagt BestelBewuster veel tijd van gebruikers van de tool?',
        content: 'Nee, de software is gemakkelijk te implementeren binnen een bedrijf. Daarnaast zorgt de tool er juist voor dat bij organisatoren van meetings tijd wordt bespaard. Als uitgenodigde van een meeting vraagt BestelBewuster met een paar klikken zelf aan te geven wat je wilt lunchen.',
      },
      {
        title: 'Hoe gaat BestelBewuster om met persoonsgegevens?',
        content: 'Er worden geen gevoelige gegevens opgeslagen. De tool werkt op basis van een e-mailadres en eventueel aangeven lunchbestelling. Deze gegevens zullen direct geanonimiseerd worden via encryptie en hashing. BestelBewuster is AVG bestendig en de software is opgebouwd volgens de Europese richtlijnen en met de benodigde certificaten.',
      },
    ]
    return (
      <div className="faq">
        <div className="home-container">
          <div className="h1 white bold">{t('faq')}</div>
          <br />
            {items.map((item, i) => (
              <Collapsable
                key={`collapse-${i}`}
                item={item}
                isOpen={isOpen === i}
                openCollapse={() => this.setState({isOpen: isOpen === i ? undefined : i})}
              />
            ))}
        </div>
      </div>
    )
  }
}


export default withTranslation()(FAQ)
