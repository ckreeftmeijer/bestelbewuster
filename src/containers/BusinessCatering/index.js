import React from 'react'

import CTA from '../../components/CTA'
import Trinity from '../../components/Trinity'
import FaqComponent from '../../components/FaqComponent'
import ContainerWithImage from '../../components/ContainerWithImage'
import FromPricing from '../../components/FromPricing'
import Intro from '../../components/Intro'
import Container from '../../components/Container'
import Card from '../../components/Card'
import Spacer from '../../components/Spacer'
import SimpleSteps from '../../components/SimpleSteps'
import SliderCard from '../../components/CardSlider/SliderCard'

import Advantages from '../../images/advantages.png'

const BusinessCatering = ({ history }) => {
  return (
    <div className="page-container">
      <Intro
        title="Zakelijke catering in het nieuwe normaal"
        form="nieuwe normaal"
      >
        <div>Corona heeft elk bedrijf zwaar getroffen in de lockdown periode. Echter nu de regels weer versoepeld worden zijn de bezoekersaantallen lager. BestelBewuster helpt om de uitgaven per bezoeker te verhogen en hiermee omzet verliezen te compenseren. Daarnaast speelt de 1,5-meter afstand, bewustzijn met voedsel en hygiëne in restaurants een nog veel belangrijkere rol dan voor corona</div>
      </Intro>
      <CTA
        inverted
        title="Bewust en vers aanbod vergroten zonder te verspillen en direct meer horeca omzet"
        href="mailto:info@bestelbewuster.nl"
      />
      <Container>
        <Trinity>
          {[
            { title: 'Bewust reserveren', text: 'Om de 1,5-meter afstand te bewaren, aantal bewegingen binnen het gebouw klein te houden en de druk op de cateringorganisatie en inkoop beheersbaar te houden is het noodzakelijk om tijdsloten en plekken te reserveren. Dit kan flexibel en op maat in te richten via het BestelBewuster platform.', },
            { title: 'Bewust bestellen', text: 'Om het aantal afrekenmomenten aan de kassa of aan tafel te verminderen is er de mogelijkheid om via de online cloudportal het gehele (banqueting)menu te integreren en deze in combinatie met een betaalmodule in te richten. Hierdoor kunnen al uw bezoekers, medewerkers en klanten eenvoudig vooraf betalen en in een (specifiek moment) de producten komen afhalen.', },
            { title: 'Bewust leveren', text: 'Er gebeurt veel in de food delivery markt. Het nieuwe normaal biedt kansen voor innovatie. Wil je los van alle bovenstaande argumenten ook een extra service bieden aan al uw bezoekers, medewerkers en klanten door de producten te leveren op hun werkplek of pick-up point dan kan dit natuurlijk ook.', },
          ]}
        </Trinity>
        <Spacer amount={18} vertical />
        <div className="green h3">De oplossing: Bewust en veilig omzetverhogende catering aanbieden</div>
        <Spacer amount={8} vertical />
        <div className="container margin--lg">
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={1}
              title="Voordelen voor bezoekers"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Bewuster kiezen</li>
                  <li>Digitaal menuaanbod en beleving van restaurant</li>
                  <li>Veilig en tevreden genieten van het cateringaanbod</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={2}
              title="Voordelen voor de horeca in het park"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Inkoopmanagement verbeteren</li>
                  <li>Omzet verhogen</li>
                  <li>Bezoekersstromen reguleren</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={3}
              title="Voordelen voor het leisurepark"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Veiligheid binnen het nieuwe normaal waarborgen</li>
                  <li>Het aanbod vers en gezond vergroten zonder verspilling</li>
                  <li>Duurzame exposure</li>
                </ul>
              </div>
            </Card>
          </div>
        </div>
      </Container>
      <Spacer amount={20} vertical />
      <Container>
        <div className="green h3">Hoe het werkt z</div>
        <Spacer amount={7} vertical />
        <SimpleSteps
          steps={[
            {title: 'Communicatie kanalen', text: 'Bezoeker kan online via een link of bijvoorbeeld een QR code een bestelling plaatsen uit het beschikbare menu'},
            {title: 'Bewuste voorkeuren', text: 'Bezoeker kiest een tijd en datum uit de beschikbare tijdsloten'},
            {title: 'Reward / incentive', text: 'Bezoeker kiest voor afhalen, bezorgen of eten in het (bedrijfs)restaurant'},
            {title: 'Consumeren', text: 'De bestelling wordt direct afgerekend, achteraf betaald aan de kassa, of gaat op kostenplaats'},
            {title: 'Analyse', text: 'Cateraar ontvangt bestellingen via dashboard, mail of gewenst communicatiekanaal'},
          ]}
        />
      </Container>
      <Spacer amount={8} vertical />
      <CTA
        inverted
        title="Bewust en vers aanbod vergroten zonder te verspillen en direct meer horeca omzet"
        href="mailto:info@bestelbewuster.nl"
      />
      <Spacer amount={10} vertical />
      <ContainerWithImage image={Advantages}>
        <>
          <div className="green h3">De unieke voordelen</div>
          <div className="pro-side no-padding">
          <ul>
            <li>Huisstijl volledig inregelbaar </li>
            <li>Digitalisering catering</li>
            <li>Data en inzichten via dashboard</li>
            <li>Plug and play software</li>
            <li>Review mogelijkheden</li>
            <li>Koppelen met kassa optioneel</li>
            <li>Privacy en security safe</li>
          </ul>
          </div>
        </>
      </ContainerWithImage>
      <Spacer amount={8} vertical />
      <FromPricing
        inverted
        title="Vanaf € 50,- per maand"
        href="mailto:info@bestelbewuster.nl"
      />
      <div>
        <div style={{ maxWidth: '500px', margin: '0 auto', cursor: 'pointer' }}>
          <Spacer amount={8} vertical />
          <Card>
            <SliderCard
              title="Zakelijke Evenementen"
              handleClick={() => history.push("/oplossingen/zakelijke-evenementen")}
              letter={'Z'}
              index={1}
            >
              Organiseer jij zakelijke evenementen waarbij je rekening wilt houden wilt houden met de 1,5 meter samenleving? Check ook onze zakelijke evenementen pagina voor meer info.
            </SliderCard>
          </Card>
        </div>
      </div>
      <Spacer amount={10} vertical />
      <FaqComponent
        items={[
          {
            title: 'Moet een bezoeker per se online afrekenen?',
            content: 'Nee, dit hoeft niet. Er kan gekozen voor het plaatsen van de bestelling en dat het afrekenen gebeurd aan de kassa. Bij dit laatste houdt de software rekening met het voorkomen van te lange rijen bij de kassa.'
          },
          {
            title: 'Moet ik alle features afnemen?',
            content: 'Nee, je kunt elke feature los of in de combinatie afnemen. Sommige klanten kiezen voor de afhaal functie met afrekenen, waar andere enkele de tijdsloten in een bedrijfsrestaurant functie gebruiken. Een combinatie van alle features is ook mogelijk. Het systeem is volledig flexibel.'
          },
          {
            title: 'Hoe helpen jullie met meer omzet?',
            content: 'Een servicegerichte digitale oplossing helpt voor een restaurant of cateraar om te kunnen concurreren. Gemak dient de medewerker!'
          },
          {
            title: 'Worden de gegevens van bezoekers opgeslagen?',
            content: 'Nee, de software van BestelBewuster is volledig privacy en security veilig. Gegevens worden direct encrypted en nadat het bezoek is geweest verwijderd.'
          },
          {
            title: 'Hoe wordt verspilling voorkomen via jullie systeem?',
            content: 'BestelBewuster heeft algemene data over bezoek en consumptie. Dit vullen wij aan met bezoekersprofielen die wij maken op dagniveau. Dit profiel komt onder andere tot stand door de bestellingen op dagniveau.'
          },
          {
            title: 'Hoe worden bezoekersstromen gereguleerd?',
            content: 'Via tijdsloten en maximale bezetting wordt er gezorgd dat er niet teveel bezoekers tegelijkertijd in bijvoorbeeld het restaurant zijn. Het van te voren online afrekenen zorgt ervoor dat te lange rijen bij de kassa wordt voorkomen. Bezorgen tot aan het bureau vermindert activiteit in de wandelgangen. Daarnaast draagt dit allen bij aan de optimale personeelsbezetting gedurende de dag in bijvoorbeeld het bedrijfsrestaurant.'
          },

        ]}
      />
    </div>
  )
}

export default BusinessCatering
