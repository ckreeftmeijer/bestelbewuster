import React from 'react'

import CTA from '../../components/CTA'
import Trinity from '../../components/Trinity'
import FaqComponent from '../../components/FaqComponent'
import ContainerWithImage from '../../components/ContainerWithImage'
import FromPricing from '../../components/FromPricing'
import Intro from '../../components/Intro'
import Container from '../../components/Container'
import Card from '../../components/Card'
import Spacer from '../../components/Spacer'
import SimpleSteps from '../../components/SimpleSteps'
import SliderCard from '../../components/CardSlider/SliderCard'


import RollerCoaster from './rollercoaster.png'
import Advantages from '../../images/advantages.png'

const DayTrips = ({ history }) => {
  return (
    <div className="page-container">
      <Intro
        title="LEISURE PARKEN"
        claim="Bewust en vers aanbod vergroten zonder te verspillen en direct meer horeca omzet"
        image={RollerCoaster}
        form="pretparken"
      >
        <div>Restaurants in pretparken, dierentuinen en musea lopen tot 20 procent omzet mis doordat mensen eten van thuis meenemen naar een dagje uit.</div>
        <br />
        <div>BestelBewuster software zorgt voor extra omzet terwijl verspilling wordt voorkomen. Daarnaast wordt het aanbod bewuster en de inkoop duurzamer.</div>
      </Intro>
      <CTA
        inverted
        title="Bewust en vers aanbod vergroten zonder te verspillen en direct meer horeca omzet"
        href="mailto:info@bestelbewuster.nl"
      />
      <Container>
        <Trinity>
          {[
            { title: 'Bewust informeren', text: 'De helft van de bezoekers oriënteert zich van te voren online over het horeca aanbod. Met bestelbewuster heb je automatisch een persoonlijk contactmoment om hierover bewust te informeren. Gebruik dit moment om te vertellen over voedingswaarden, herkomst informatie of bijvoorbeeld over het aanbod voor de vegetariër.', },
            { title: 'Vers aanbod', text: 'Het aanbieden van vers voedsel gaat al snel gepaard met meer voedselverspilling. Met bestelbewuster software is er geen reden meer om niet vers en bewust aan te bieden. De inkoop blijft efficiënt. Daarnaast draagt het aanbieden van vers voedsel bij aan de keuze van bezoekers om tijdens een dagje uit gezonder te consumeren.', },
            { title: 'Omzet verhogen via korting', text: 'De voornaamste reden voor bezoekers om eigen consumpties mee te nemen naar een dagje uit is de prijs. Door de bezoekers te belonen wanneer van te voren de gewenste consumpties aangegeven worden stimuleert BestelBewuster het thuis laten van de eigen consumpties.', },
          ]}
        </Trinity>
        <Spacer amount={18} vertical />
        <div className="green h3">De oplossing: Minder verspilling en meer omzet bij dagjes uit bezoekers</div>
        <Spacer amount={8} vertical />
        <div className="container margin--lg">
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={1}
              title="Voordelen voor bezoekers"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Bewuster kiezen</li>
                  <li>Digitaal menuaanbod en beleving van restaurant</li>
                  <li>Met korting op catering bijdragen aan minder verspilling</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={2}
              title="Voordelen voor de horeca in het park"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Inkoopmanagement verbeteren</li>
                  <li>Omzet verhogen</li>
                  <li>Bezoekersstromen reguleren</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={3}
              title="Voordelen voor het leisurepark"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Digitaliseren van cateringprocessen</li>
                  <li>Het aanbod vers en gezond vergroten zonder verspilling</li>
                  <li>Meer omzet generen binnen horeca</li>
                </ul>
              </div>
            </Card>
          </div>
        </div>
      </Container>
      <Spacer amount={20} vertical />
      <Container>
        <div className="green h3">Hoe het werkt voor de bezoeker</div>
        <Spacer amount={7} vertical />
        <SimpleSteps
          steps={[
            {title: 'Communicatie kanalen', text: 'Bezoeker heeft kaartje gekocht en kan via de bevestigingsmail catering voorkeuren aangeven via BestelBewuster website'},
            {title: 'Bewuste voorkeuren', text: 'Bezoeker geeft cateringvoorkeuren aan. (optioneel:tijd en plaats)'},
            {title: 'Reward / incentive', text: 'Bezoeker ontvangt een kortingsvoucher voor de consumpties'},
            {title: 'Consumeren', text: 'Bezoeker geniet van dagje uit en bewuste consumpties'},
            {title: 'Analyse', text: 'Verrijking transactiedata met bezoekers voorkeuren'},
          ]}
        />
      </Container>
      <Spacer amount={8} vertical />
      <CTA
        title="Ook als leisurepark het duurzame voorbeeld geven samen met optimale food service, neem contact op:"
        href="mailto:info@bestelbewuster.nl"
        inverted
      />
      <Spacer amount={10} vertical />
      <ContainerWithImage image={Advantages}>
        <>
          <div className="green h3">De unieke voordelen</div>
          <div className="pro-side no-padding">
            <ul>
              <li>Huisstijl volledig inregelbaar </li>
              <li>Digitalisering catering</li>
              <li>Data en inzichten via dashboard</li>
              <li>Plug and play software</li>
              <li>Review mogelijkheden</li>
              <li>Koppelen met ticketingsysteem </li>
              <li>Privacy en security safe</li>
            </ul>
          </div>
        </>
      </ContainerWithImage>
      <Spacer amount={8} vertical />
      <FromPricing
        inverted
        title="Vanaf € 50,- per maand"
        href="mailto:info@bestelbewuster.nl"
      />
      <div>
        <div style={{ maxWidth: '500px', margin: '0 auto', cursor: 'pointer' }}>
          <Spacer amount={8} vertical />
          <Card>
            <SliderCard
              title="Evenementen"
              handleClick={() => history.push("/oplossingen/zakelijke-evenementen")}
              letter={'E'}
              index={1}
            >
              Heeft jouw dagje uit locatie ook verhuur van ruimtes aan groepen met (zakelijke) catering? Zie ook onze oplossing voor zakelijke evenementen.
            </SliderCard>
          </Card>
        </div>
      </div>
      <Spacer amount={10} vertical />
      <FaqComponent
        items={[
          {
            title: 'Worden de gegevens van bezoekers opgeslagen?',
            content: 'Nee, de software van BestelBewuster is volledig privacy en security veilig. Gegevens worden direct encrypted en nadat het bezoek is geweest verwijderd.'
          },
          {
            title: 'Moeten bezoekers verplicht een lunch keuze opgeven?',
            content: 'Nee, de systemen staan los van elkaar. Een bezoeker wordt vanuit de verspilling gedachte gevraagd of die van te voren een keuze wil opgeven voor bijvoorbeeld lunch en/of ontbijt . Daarbij kan hij een beloning ontvangen in bijvoorbeeld korting voor het restaurant van voorkeur. Dit is geen verplichting maar een vraag. De vraag wordt gesteld via een aparte mail of er staat een link op bijvoorbeeld het ticket van een klant. '
          },
          {
            title: 'Hoe wordt verspilling voorkomen via jullie systeem?',
            content: 'BestelBewuster heeft algemene data over bezoek en consumptie. Dit vullen wij aan met bezoekersprofielen die wij maken op dagniveau. Dit profiel komt bijvoorbeeld tot stand door het uitvragen van bijvoorbeeld restaurant voorkeur bij bezoekers. Daarnaast wordt ook indirecte verspilling voorkomen. Het doel is om bezoekers te laten consumeren in het parken en daarmee eigen consumpties thuis laten. Mensen gooien veel “rugzak” artikelen aan het einde van de dag onverhoopt weg.'
          },
          {
            title: 'Waarom zou een bezoeker van te voren hieraan meewerken?',
            content: 'Duurzaamheid wordt met de dag belangrijker. Zowel voor bedrijven als consumenten. Door dit te communiceren zijn de eerste bezoekers bereid om mee te werken. Daarnaast kan er bijvoorbeeld gewerkt worden met een beloningssysteem. Denk aan een kortingsvoucher die je uitgeeft voor het voorkeur restaurant aangegeven door de bezoeker. Daarmee voorkom je niet alleen verspilling, maar verhoog je ook omzet.'
          },
          {
            title: 'Hoe kunnen reviews helpen met meer omzet?',
            content: 'Vriendelijkheid personeel en hygiëne zijn zaken die aangegeven worden als belangrijk voor een bezoeker om naar horeca op park te gaan. Reviews waarborgen kwaliteit en leidt tot vertrouwen.  Via het bestelbewuster review systeem kan dit gemeten worden. De resultaten uit reviews kunnen bij nieuwe bezoekers in het keuze moment gedeeld worden. Hierdoor hebben zij voor het parkbezoek al een vertrouwd gevoel.'
          },
          {
            title: 'Hoe worden bezoekersstromen gereguleerd?',
            content: 'Bezoekers sturen door het park door van te voren een lunch locatie toe te wijzen. Bijvoorbeeld door aan een kortingsvoucher een tijd en plaats te koppelen. Daarnaast kan ook met ontbijt aanbod gekeken worden of een deel van de bezoekers vroeger in het park komt. Hiermee wordt direct de ontbijt omzet gepushed. Door bezoekersstromen in restaurant gedurende de dag te sturen zorgt ervoor dat restaurants langer open kunnen blijven met een efficiënte bezetting.'
          },
          {
            title: 'Hoe draagt BestelBewuster bij dat bezoekers gezonder eten in park?',
            content: 'Het aanbod van gezond voedsel is veelal laag binnen leisure omdat de vraag laag is. Het van te voren maken van een keuze via BestelBewuster draagt bij aan meer gezonde vraag door bezoeker. Vooraf wordt namelijk een meer bewuste en gezonde keuze gemaakt dan op een moment zelf. '
          }

        ]}
      />
    </div>
  )
}

export default DayTrips
