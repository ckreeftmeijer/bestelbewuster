import React from 'react'

import CTA from '../../components/CTA'
import Trinity from '../../components/Trinity'
import FaqComponent from '../../components/FaqComponent'
import ContainerWithImage from '../../components/ContainerWithImage'
import FromPricing from '../../components/FromPricing'
import Intro from '../../components/Intro'
import Container from '../../components/Container'
import Card from '../../components/Card'
import Spacer from '../../components/Spacer'
import SimpleSteps from '../../components/SimpleSteps'
import SliderCard from '../../components/CardSlider/SliderCard'

import Advantages from '../../images/advantages.png'

const MKB = ({ history }) => {
  return (
    <div className="page-container">
      <Intro
        title="MKB"
        form="MKB"
      >
        <div>Is het bij jullie op kantoor ook altijd een grote uitdaging om de lunch wensen van je collega’s op te nemen? En blijft er vaak eten over wat uiteindelijk weggegooid wordt? De kantoorlunch kan eenvoudiger en een stuk efficiënter.</div>
        <br />
        <div>BestelBewuster software zorgt voor extra inzicht in de wensen en dieetwensen van je collega’s. Daarnaast wordt het aanbod bewuster en de wekelijkse inkopen duurzamer.</div>
      </Intro>
      <CTA
        inverted
        title="Neem contact op: Minder verspilling in voedsel en optimale persoolsbezetting"
        href="mailto:info@bestelbewuster.nl"
      />
      <Container>
        <Trinity>
          {[
            { title: 'Wie is op kantoor en luncht mee', text: 'Nu steeds meer mensen thuiswerken is het elke dag weer onzeker hoeveel collega’s op kantoor aanwezig zijn en of ze blijven lunchen. Dit maakt het zeer lastig om met iedereen rekening te houden met de lunch. Via de BestelBewuster software wordt dit inzichtelijk doordat iedereen zijn/of haar wensen kan aangeven. Servicegericht, maar zonder het vervelende van te veel bestellen.', },
            { title: 'Persoonlijker en bewuster aanbod naar medewerkers, bezoekers en klanten', text: 'Doordat er steeds meer mensen bewuster willen leven en hierdoor de overstap maken naar vegetarisch of zelfs veganistisch voedsel lopen de wensen per persoon steeds verder uiteen. Worden jouw bewuste wensen ook altijd vergeten tijdens de kantoor lunch? Hoe prettig zou het zijn als je deze wensen eenvoudig door kan geven aan de office manager? Via de BestelBewuster software wordt dit inzichtelijk doordat de werknemer vooraf zijn wensen doorgeeft.', },
            { title: 'Efficiënter inkoopbeleid ', text: 'Laat je werknemers zien dat het voorkomen van verspilling belangrijk is voor het bedrijf. Laat je collega’s een bewuste keuze maken door aan te geven wanneer ze aanwezig zijn en wat hun eet wensen zijn. Samen een bijdrage aan minder verspilling bij de dagelijkse of wekelijkse catering op kantoor.', },
          ]}
        </Trinity>
        <Spacer amount={18} vertical />
        <div className="green h3">De oplossing: Minder verspilling en efficiënt boodschappen doen op kantoor</div>
        <div className="container margin--lg">
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={1}
              title="Voordelen voor de medewerker"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Bewuster en persoonlijker</li>
                  <li>Gezonde keuze stimuleren </li>
                  <li>Gemakkelijk aangeven of je aanwezig bent op kantoor</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={2}
              title="Voordelen voor de office manager"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Tijdbesparing wekelijkse boodschappen</li>
                  <li>Minder verspilling bij catering door up to date bestellijst</li>
                  <li>Alles digitaal te regelen via plugin</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={3}
              title="Voordelen voor de zakelijke klant"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Service naar alle medewerkers</li>
                  <li>Het juiste voorbeeld als MKB’er</li>
                  <li>Inkoopmanagement verbeteren</li>
                </ul>
              </div>
            </Card>
          </div>
        </div>
      </Container>
      <Spacer amount={20} vertical />
      <Container>
        <div className="green h3">Hoe het werkt</div>
        <Spacer amount={7} vertical />
        <SimpleSteps
          steps={[
            {title: 'Communicatie kanalen', text: 'Office manager koppelt de boodschappenlijst van de gewenste leverancier'},
            {title: 'Bewuste voorkeuren', text: 'Medewerker meld zich aan wanneer hij of zij op kantoor aanwezig is'},
            {title: 'Reward / incentive', text: 'Medewerker krijgt een e-mail of whatsapp om aan te geven of ze aanwezig zijn en wat ze willen eten'},
            {title: 'Consumeren', text: 'Kantoor ontvangt lijst met aanwezigen per dag en optimale boodschappenlijst'},
            {title: 'Analyse', text: 'Boodschappen kunnen besteld worden rekening houdend met de dieetwensen'},
          ]}
        />
      </Container>
      <Spacer amount={8} vertical />
      <CTA
        title="Ook meedoen aan MKB catering zonder voedselverspilling, neem contact op:"
        href="mailto:info@bestelbewuster.nl"
        inverted
      />
      <Spacer amount={10} vertical />
      <ContainerWithImage image={Advantages}>
        <>
          <div className="green h3">De unieke voordelen</div>
          <div className="pro-side no-padding">
            <ul>
              <li>Huisstijl volledig inregelbaar </li>
              <li>Digitalisering inkoop</li>
              <li>Data en inzichten via dashboard</li>
              <li>Plug and play software</li>
              <li>Privacy en security safe</li>
            </ul>
          </div>
        </>
      </ContainerWithImage>
      <Spacer amount={8} vertical />
      <FromPricing
        inverted
        title="Vanaf € 50,- per maand"
        href="mailto:info@bestelbewuster.nl"
      />
      <div>
        <div style={{ maxWidth: '500px', margin: '0 auto', cursor: 'pointer' }}>
          <Spacer amount={8} vertical />
          <Card>
            <SliderCard
              title="Co-working"
              handleClick={() => history.push("/segmenten/co-working")}
              letter={'C'}
              index={1}
            >
              Zijn jullie met het bedrijf onderdeel van een co-working? Check ook de “co-working” pagina.
            </SliderCard>
          </Card>
        </div>
      </div>
      <Spacer amount={10} vertical />
      <FaqComponent
        items={[
          {
            title: 'Op welke manier wordt tijd bespaart via jullie software?',
            content: 'Een office manager of andere verantwoordelijke voor de wekelijkse zakelijke boodschappen is over het algemeen te veel tijd kwijt aan deze bestellen. Zeker wanneer die met aanwezigheid en wensen probeert rekening te houden. Met de software automatiseren wij dit proces.'
          },
          {
            title: 'Voor welke systemen is de plugin te gebruiken?',
            content: 'De plugin werkt samen met de bekende e-mailclients van Outlook en Gmail. Daarnaast is een de online omgeving ook aan te spreken via de browser.'
          },
          {
            title: 'Moeten wij werken met een plugin?',
            content: 'Nee dit is niet persé nodig. Wel zitten hier veel voordelen aan omdat met de plugin er via de agenda aanwezigheid gemonitord kan worden en deze up to date blijft. Het alternatief is dat elke medewerker handmatig deze gegevens invult in een online omgeving van BestelBewuster.'
          },
          {
            title: 'Waarom zou een medewerker van te voren hieraan meewerken?',
            content: 'Duurzaamheid wordt met de dag belangrijker. Zowel voor bedrijven als consumenten. Door dit te communiceren zijn de medewerkers bereid om mee te werken. Zo voelt iedereen zich verantwoordelijk om een bewuste keuze te maken.'
          },
          {
            title: 'Moeten medewerkers verplicht een lunch keuze opgeven?',
            content: 'Nee, de systemen staan los van elkaar. Een medewerker wordt vanuit de verspilling gedachte gevraagd of die van te voren een keuze wil opgeven. Dit is geen verplichting maar een vraag. De vraag wordt gesteld via een aparte mail of er staat een link op bijvoorbeeld de kantoor sharepoint.'
          },
          {
            title: 'Hoe wordt verspilling voorkomen via jullie systeem?',
            content: 'BestelBewuster heeft algemene data over aanwezigheid en consumptie. Te samen met de voorkeur van de medewerker wordt er een lunch profiel gemaakt op dagniveau rekening houdend met hoeveel medewerkers aanwezig zijn en hun dieetvoorkeuren. '
          },
        ]}
      />
    </div>
  )
}

export default MKB
