import React from 'react'

import CTA from '../../components/CTA'
import Trinity from '../../components/Trinity'
import FaqComponent from '../../components/FaqComponent'
import ContainerWithImage from '../../components/ContainerWithImage'
import FromPricing from '../../components/FromPricing'
import Intro from '../../components/Intro'
import Container from '../../components/Container'
import Card from '../../components/Card'
import Spacer from '../../components/Spacer'
import SimpleSteps from '../../components/SimpleSteps'
import SliderCard from '../../components/CardSlider/SliderCard'

import Advantages from '../../images/advantages.png'

const Hotels = ({ history }) => {
  return (
    <div className="page-container">
      <Intro
        title="HOTELS"
        form="hotels"
      >
        <div>Het ontbijtbuffet bij hotels is niet meer zoals het vroeger was. Een hotelgast verwacht maatwerk en dit is dus een vereiste vanuit een hotel.</div>
        <br />
        <div>BestelBewuster software zorgt voor dit maatwerk zonder verspilling. De hotelgast wordt via een digitale bestelflow betrokken bij de keuze voor ontbijt, lunch en diner. </div>
      </Intro>
      <CTA
        inverted
        title="Digitale catering- en roomservice naar hotelgasten zonder verspilling"
        href="mailto:info@bestelbewuster.nl"
      />
      <Container>
        <Trinity>
          {[
            { title: 'Meer service naar hotelgasten', text: 'Met de software kun je hotelgasten laten kiezen voor roomservice, afhalen of eten in het restaurant. Met BestelBewuster zijn de mogelijkheden qua ruimte in bijvoorbeeld het restaurant altijd up to date. De hotelgast kan voor het van te voren invullen beloond worden. ', },
            { title: 'Maatwerk en efficiënter inkopen', text: 'De hotelgast van nu vereist een persoonlijker en bewuster aanbod voor de maaltijden gedurende zijn bezoek. Met het digitaal eerder aangeven van lunchwensen via BestelBewuster worden deze wensen inzichtelijk zonder dat dit gepaard gaat met verspilling. Het eerder aangeven van voorkeuren is daarmee goed voor de gast, het inkoopbeleid en het imago van het hotel.', },
            { title: 'Omzet stimuleren van ontbijt, lunch en diner door gasten', text: 'Door hotelgasten van te voren te enthousiasmeren over het aanbod aan catering door de dag heen kun je hen stimuleren om gebruik te maken van de cateringmogelijkheden van het hotel. Informeren over het menu en bijbehorende voedingswaarde en herkomst draagt al bij aan de beleving. Daarnaast zorgt gemak voor meer omzet. Denk bijvoorbeeld aan de klant op zakenreis die voor bezoek al zijn diners en mogelijkheden al kan plannen.', },
          ]}
        </Trinity>
        <Spacer amount={18} vertical />
        <div className="green h3">De oplossing: Minder verspilling en meer omzet bij catering binnen hotels</div>
        <Spacer amount={8} vertical />
        <div className="container margin--lg">
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={1}
              title="Voordelen voor het restaurant"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Drukte en personeelsbezetting efficiënter inplannen</li>
                  <li>Rendabel inkoopbeleid</li>
                  <li>Hoog versaanbod zonder verspilling</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={2}
              title="Voordelen voor de bezoeker"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Bewuster kiezen</li>
                  <li>Digitaal menuaanbod en beleving van restaurant</li>
                  <li>Geen teleurstelling, altijd een passend gerecht</li>
                </ul>
              </div>
            </Card>
          </div>
          <div className="col-12 col-md-6">
            <Card
              circle
              idx={3}
              title="Voordelen voor het hotel"
            >
              <div className="pro-side no-padding">
                <ul>
                  <li>Kosten restaurant verlagen</li>
                  <li>Omzet verhogen</li>
                  <li>Duurzaam imago</li>
                </ul>
              </div>
            </Card>
          </div>
        </div>
      </Container>
      <Spacer amount={20} vertical />
      <Container>
        <div className="green h3">Hoe het werkt voor de bezoeker</div>
        <Spacer amount={7} vertical />
        <SimpleSteps
          steps={[
            {title: 'Communicatie kanalen', text: 'Hotelgast boekt overnachting'},
            {title: 'Bewuste voorkeuren', text: 'BestelBewuster stuurt digitale menukaart via e-mail naar hotelgast.'},
            {title: 'Reward / incentive', text: 'Hotelgast kan optioneel voorkeuren aangeven voor ontbijt en/of lunch en of/diner'},
            {title: 'Consumeren', text: 'Hotelrestaurant ontvangt op dagniveau optimale bestellijst voor de catering'},
            {title: 'Analyse', text: 'Hotelgast geniet eenvoudig van de cateringfaciliteiten van het hotel.'},
          ]}
        />
      </Container>
      <Spacer amount={8} vertical />
      <CTA
        title="Ook als hotel verspilling verminderen en omzet verhogen? Neem contact op!"
        href="mailto:info@bestelbewuster.nl"
        inverted
      />
      <Spacer amount={10} vertical />
      <ContainerWithImage image={Advantages}>
        <>
          <div className="green h3">De unieke voordelen</div>
          <div className="pro-side no-padding">
            <ul>
              <li>Huisstijl volledig inregelbaar </li>
              <li>Digitalisering inkoop</li>
              <li>Data en inzichten via dashboard</li>
              <li>Plug and play software</li>
              <li>Review mogelijkheden</li>
              <li>Privacy en security safe</li>
            </ul>
          </div>
        </>
      </ContainerWithImage>
      <Spacer amount={8} vertical />
      <FromPricing
        inverted
        title="Vanaf € 50,- per maand"
        href="mailto:info@bestelbewuster.nl"
      />
      <div>
        <div style={{ maxWidth: '500px', margin: '0 auto', cursor: 'pointer' }}>
          <Spacer amount={8} vertical />
          <Card>
            <SliderCard
              title="Corona"
              handleClick={() => history.push("/oplossingen/nieuwe-normaal")}
              letter={'C'}
              index={1}
            >
              Vanwege corona heeft elk hotel uitdagingen met het beleid in het hotelrestaurant en bij het ontbijtbuffet. Daarnaast speelt bewustzijn met voedsel en hygiëne in restaurants een nog veel belangrijkere rol dan voorheen. Voor het hotel helpt de BestelBewuster software met 1,5 meter samenleving? Zie voor extra informatie ook de 1,5 meter pagina.
            </SliderCard>
          </Card>
        </div>
      </div>
      <Spacer amount={10} vertical />
      <FaqComponent
        items={[
          {
            title: 'Worden de gegevens van hotelgasten opgeslagen?',
            content: 'Nee, de software van BestelBewuster is volledig privacy en security veilig. Gegevens worden direct encrypted en nadat het bezoek is geweest verwijderd.'
          },
          {
            title: 'Is de software alleen beschikbaar voor hotelgasten?',
            content: 'Nee, naast hotelgasten zijn er natuurlijk ook gasten die van buitenaf naar het hotelrestaurant komen. De software is ook toepasbaar voor deze gasten. Dit kan onafhankelijk of samen met elkaar gebruikt worden.'
          },
          {
            title: 'Hoe stimuleert jullie software de omzet?',
            content: 'Bezoeker oriënteren  zich voorafgaand aan het hotelbezoek op het algehele catering aanbod. Hoe is het aanbod qua vers en wat zijn ingrediënten zijn veelgestelde vragen. Door voorafgaand in de klantreis al aanwezig te zijn en een keuze te stimuleren wordt er ingespeeld op een latente behoefte. Anders gaat de bezoeker in de waan van de dag bijvoorbeeld bij het koffietentje om de hoek zitten.'
          },
          {
            title: 'Hoe wordt verspilling voorkomen via jullie software?',
            content: 'BestelBewuster heeft algemene data over hotel bezoek en catering. Dit vullen wij aan met de informatie die . Dit profiel komt bijvoorbeeld tot stand door het uitvragen van de voorkeur voor ontbijt, lunch en/of diner. Wanneer een hotel ervoor kiest om hotelgasten van te voren de bestelling te geven ontstaat 100% maatwerk. De kans op verspilling wordt voor het hotel minimaal.'
          },
          {
            title: 'Waarom is een duurzaam imago belangrijk voor mijn hotel?',
            content: 'Voor de keuze voor jouw hotel door een hotelgast of bezoeker van het hotelrestaurant spelen verschillende zaken een rol. Eén van de zaken heeft betrekking op het hotel en duurzaamheid. Wat een hotel aan duurzaamheid hieraan hieraan doet speelt een steeds belangrijkere rol in de beslissing. '
          },
        ]}
      />
    </div>
  )
}

export default Hotels
